#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "huffman.h"
#include "debug.h"


#define CODE_MAX_LENGTH 16
#define NONE 0xFF

struct huff_node {
    struct huff_node *left;
    struct huff_node *right;

    uint8_t value;
};

struct huff_table {
    struct huff_node *root;
};

/**
 * Créé un noeud pour l'arbre de Huffman
 *
 * @return struct huff_node* Le noeud créé
 */
static inline struct huff_node *createNode() {
    struct huff_node *node = malloc(sizeof(struct huff_node));
    if(!node){
      fatalError("Failed to allocate memory");
    }
    node->value = NONE;
    node->left  = NULL;
    node->right = NULL;
    return node;
}

/**
 * Libère un noeud d'un arbre de Huffman
 *
 * @param struct huff_node* Le noeud à libérer
 */
static void freeNode(struct huff_node *node) {
    if(node->left != NULL)
        freeNode(node->left);

    if(node->right != NULL)
        freeNode(node->right);

    free(node);
}

/**
 * Descend dans l'arbre du Huffman jusqu'à trouver un code valide
 *
 * @param struct huff_node* Le noeud courant
 * @param struct bitstream* Le flux de données
 * @return int8_t La valeur décodée
 */
int8_t nextValue(struct huff_node *node, struct bitstream *stream) {
    // C'est un noeud feuille, le code a été trouvé
    if(node->value != NONE) {
        return node->value;

    // C'est un noeud interne, on descend dans l'arbre
    } else {
        uint32_t res = 0;
        if (!read_bitstream_safe(stream, 1, &res, true)) {
            fatalError("Invalid Huffman sequence in bitstream : 1 bit required");
        }

        struct huff_node *nextNode = (res) ? node->right : node->left;

        // Le code n'est pas reconnu. ABORT ABORT ABORT !!
        if(nextNode == NULL) {
            fatalError("Invalid Huffman sequence in bitstream");
        }

        return nextValue(nextNode, stream);
    }
}

/**
 * Construit un niveau de l'arbre de Huffman
 *
 * @param struct bitstream* Le flux de données
 * @param struct huff_node* La racine du sous-arbre
 * @param uint8_t           Le niveau de l'arbre (aka. La profondeur à construire)
 * @param uint8_t*          Le nombre de code à construire pour ce niveau
 */
static void buildLevel(struct bitstream *stream, struct huff_node *root, uint8_t level, uint8_t *nbCode) {
    // On est au bon niveau, on construit le code si le noeud est libre
    if(level == 0 && root->value == NONE && (*nbCode) > 0) {
        uint32_t res = 0;
        if (!read_bitstream_safe(stream, 8, &res, true)) {
            fatalError("Invalid Huffman value in bitstream : 8 bits required");
        }
        root->value = (uint8_t)res;
        (*nbCode)--;

    // On est sur un noeud interne, pas encore au bon niveau, on continue de descendre dans l'arbre
    } else if(level > 0 && root->value == NONE) {
        if(root->left == NULL)
            root->left = createNode();

        if(root->right == NULL)
            root->right = createNode();

        // On construit le niveau inférieur sur les fils
        buildLevel(stream, root->left, (uint8_t)(level - 1), nbCode);
        buildLevel(stream, root->right, (uint8_t)(level - 1), nbCode);
    }
}

/**
 * Charge un arbre de Huffman
 *
 * @param struct bitstream* Le flux de données bien positionné
 * @param uint16_t*         Le nombre d'octets lu pour construire l'arbre
 * @return struct huff_table* L'arbre de Huffman construit
 */
struct huff_table *load_huffman_table(struct bitstream *stream, uint16_t *nb_byte_read) {
    struct huff_table *table = malloc(sizeof(struct huff_table));
    if(!table){
      fatalError("Failed to allocate memory.");
    }
    uint8_t nbCodes[CODE_MAX_LENGTH];

    // Enumération du nombre de codes pour chaque longueur
    uint16_t totalCode = 0;
    for(uint8_t i = 0; i < CODE_MAX_LENGTH; i++) {
        uint32_t res = 0;
        if (!read_bitstream_safe(stream, 8, &res, true)) {
            fatalError("Wrong number of Huffman codes");
        }
        nbCodes[i] = (uint8_t)res;
        totalCode += nbCodes[i];
    }

    table->root = createNode();

    // Construction de l'arbre
    for(uint8_t i = 0; i < CODE_MAX_LENGTH; i++) {
        if(nbCodes[i])
            buildLevel(stream, table->root, (uint8_t)(i + 1), &nbCodes[i]);
    }

    *nb_byte_read = (uint16_t)(CODE_MAX_LENGTH + totalCode);
    return table;
}

/**
 * Retourne la valeur suivante du bitstream décodée par l'arbre de Huffman
 * passé en paramètre
 *
 * @param struct huff_table*    L'arbre de Huffman qui servira au décodage
 * @param struct bitstream*     Le flux de données
 * @return int8_t La valeur décodée
 */
int8_t next_huffman_value(struct huff_table *table, struct bitstream *stream) {
    return nextValue(table->root, stream);
}

/**
 * Libère un arbre de Huffman en mémoire
 *
 * @param struct huff_table* La table à libérer
 */
void free_huffman_table(struct huff_table *table) {
    if(table != NULL) {
        freeNode(table->root);
        free(table);
        table = NULL;
    }
}

/**
 * Cherche le code à partir du noeud node. Si trouvé, renvoie True, sinon False.
 *
 * @param struct huff_node*     Le noeud de départ
 * @param uint16_t              La valeur à trouver dans l'arbre
 * @param uint16_t*             Le code de Huffman correspondant
 * @param uint8_t*              La taille du code de Huffman
 * @return bool                 True si le code a été trouvé dans cette partie de l'arbre
 */
static bool findCode(struct huff_node *node, uint16_t value, uint16_t *code, uint8_t *length) {
    if (node->value != NONE && node->value == value) {
        return true;
    }

    uint8_t length_temp = *length;
    (*length)++;
    uint16_t code_temp = *code;
    *code <<= 1;
    if (node->left != NULL && findCode(node->left, value, code, length)) {
        return true;
    }
    *code = (code_temp << 1) | 1;
    *length = length_temp + 1;
    if (node->right != NULL && findCode(node->right, value, code, length)) {
        return true;
    }
    *code = code_temp;
    *length = length_temp;
    return false;
}

/**
 * Retourne le code de Huffman correspondant à la valeur fournie dans l'arbre
 *
 * @param struct huff_table*        La table de Huffman à utiliser
 * @param uint16_t                  La valeur à trouver dans l'arbre
 * @param uint8_t*                  La longueur du code de Huffman trouvé
 * @return uint16_t                 Le code de Huffman correspondant
 */
uint16_t find_huffman_code(struct huff_table *table, uint16_t value, uint8_t *code_length) {
    uint16_t code = 0;
    *code_length = 0;

    if (!findCode(table->root, value, &code, code_length)) {
        printf("Valeur : %x\n", value);
        fatalError("Value not found");
    }
    return code;
}

/**
 * Construit un niveau de l'arbre de Huffman stocké en mémoire
 *
 * @param uint8_t**         Un pointeur contenant l'adresse du tableau en mémoire
 * @param struct huff_node* La racine du sous-arbre
 * @param uint8_t           Le niveau de l'arbre (aka. La profondeur à construire)
 * @param uint8_t*          Le nombre de code à construire pour ce niveau
 */
static void buildLevel_memory(uint8_t **tab, struct huff_node *root, uint8_t level, uint8_t *nbCode) {
    // On est au bon niveau, on construit le code si le noeud est libre
    if (level == 0 && root->value == NONE && (*nbCode) > 0) {
        root->value = (uint8_t)((*tab)[0]);
        (*nbCode)--;
        (*tab)++;

    // On est sur un noeud interne, pas encore au bon niveau, on continue de descendre dans l'arbre
    } else if (level > 0 && root->value == NONE) {
        if(root->left == NULL)
            root->left = createNode();

        if(root->right == NULL)
            root->right = createNode();

        // On construit le niveau inférieur sur les fils
        buildLevel_memory(tab, root->left, (uint8_t)(level - 1), nbCode);
        buildLevel_memory(tab, root->right, (uint8_t)(level - 1), nbCode);
    }
}

/**
 * Charge un arbre de Huffman stocké dans un tableau en mémoire
 *
 * @param uint8_t*          Un pointeur sur l'arbre de Huffman en mémoire
 * @param uint16_t*         Le nombre d'octets lu pour construire l'arbre
 * @return struct huff_table* L'arbre de Huffman construit
 */
struct huff_table *load_huffman_table_memory(uint8_t *tab, uint16_t *nb_byte_read) {
    struct huff_table *table = malloc(sizeof(struct huff_table));
    if(!table){
      fatalError("Failed to allocate memory");
    }
    uint8_t nbCodes[CODE_MAX_LENGTH];

    // Enumération du nombre de codes pour chaque longueur
    uint16_t totalCode = 0;
    for(uint8_t i = 0; i < CODE_MAX_LENGTH; i++) {
        nbCodes[i] = tab[i];
        totalCode += nbCodes[i];
    }

    table->root = createNode();

    // Construction de l'arbre
    uint8_t *tab_memory;
    tab_memory = (uint8_t*)(tab + CODE_MAX_LENGTH);
    for(uint8_t i = 0; i < CODE_MAX_LENGTH; i++) {
        if(nbCodes[i]) {
            buildLevel_memory(&tab_memory, table->root, (uint8_t)(i + 1), &nbCodes[i]);
        }
    }

    *nb_byte_read = (uint16_t)(CODE_MAX_LENGTH + totalCode);
    return table;
}
