#include "bitstream.h"
#include <stdlib.h>
#include "debug.h"


#define BITS_IN_BYTES 8
struct bitstream {
  FILE *source;
  uint8_t curByte;
  uint8_t posInByte;
  bool isReadyToDiscard;
};

struct bitstream *create_bitstream(const char *filename) {
  struct bitstream *stream = malloc(sizeof(struct bitstream));
  if(!stream){
    fatalError("Failed to allocate memory.");
  }
  if (filename) {
    stream->source = fopen(filename, "r");
    if (!stream->source) {
      perror("Error");
      exit(-1);
    }
  }
  stream->curByte = 0;
  stream->posInByte = 0;
  stream->isReadyToDiscard = false;
  return stream;
}

struct bitstream *create_write_bitstream(FILE *fichier) {
    struct bitstream *stream = malloc(sizeof(struct bitstream));
    if(!stream){
      fatalError("Failed to allocate memory.");
    }
    stream->source = fichier;
    stream->curByte = 0;
    stream->posInByte = 0;
    stream->isReadyToDiscard = false;
    return stream;
}

void end_write(struct bitstream *stream) {
    if(stream->posInByte != 0) {
        stream->curByte <<= (BITS_IN_BYTES - (stream->posInByte + 1));
        fwrite(&stream->curByte, 1, 1, stream->source);
    }
}

void close_bitstream(struct bitstream *stream) {
  fclose(stream->source);
  free(stream);
  stream = NULL;
}

uint8_t read_bitstream(struct bitstream *stream, uint8_t nb_bits,
                                     uint32_t *dest,
                                     bool discard_byte_stuffing) {

  if (nb_bits == 0) {
    return 0;
  }
  else if (nb_bits > 32) {
    char message[100];
    sprintf(message, "Cannot read %d bits, duuuuuh\n", nb_bits);
    fatalError(message);
  }
  //@NOTE : Explication générale de la méthode. On peut demander au maxium 32
  // bits (4octets à la fois).
  // On crée des buffers de 8 bits, qui est le nombre maximal de bits qui peut
  // être donné par un octet
  // Si aucun de ses bits n'a déjà été donné. On divise ensuite le nombre de
  // bits à fournir en sortie
  // en fonction du nombre de bits déjà utilisé de chacun. Par masque on
  // reconstitue ensuite le 32 bit.

  // Eviter de se retrouver avec une valeur déjà initialisée.
  *dest = 0;
  // Nombre total de bit à retourner.
  uint8_t bitsToYield = nb_bits;
  uint8_t currentYieldNb = 0;
  uint8_t buffers[4] = {0x00, 0x00, 0x00, 0x00};
  uint8_t offsets[4] = {0, 0, 0, 0};
  uint8_t bufferPosition = 0;

  while (bitsToYield) {
      if (!(stream->posInByte % 8)) {
          stream->curByte = 0;
          if (fread(&stream->curByte, 1, 1, stream->source) != 1) {
              break;
          }
          stream->posInByte = 0;
      }
      if (discard_byte_stuffing) {
          if (stream->isReadyToDiscard && stream->curByte == 0x00) {
              if (fread(&stream->curByte, 1, 1, stream->source) != 1) {
                  break;
              }
              stream->isReadyToDiscard = false;
              stream->posInByte = 0;
          } else {
              stream->isReadyToDiscard = false;
          }
          if (stream->curByte == 0xFF) {
              stream->isReadyToDiscard = true;
          }
      }

    // Si le nombre de bit que je peux donner est suffisant pour finir en une
    // passe je le fais, sinon je découpe et je donne ce que je peux.
    currentYieldNb = (bitsToYield > BITS_IN_BYTES - stream->posInByte)
                         ? BITS_IN_BYTES - stream->posInByte
                         : bitsToYield;

    // Masque qui permet de récupérer uniquement les bits dispo dans l'octet
    // courant.
    uint8_t mask = ((uint8_t)(0xFF << (BITS_IN_BYTES - currentYieldNb))) >>
                   stream->posInByte;
    // On stocke dans le buffer les bits dispos de l'octet que l'on aligne en
    // poids faibles.
    buffers[bufferPosition] |=
        ((stream->curByte & mask) >>
         (BITS_IN_BYTES - currentYieldNb - stream->posInByte));
    // On avance le curseur du nombre de bits lus dans l'octet.
    stream->posInByte += currentYieldNb;
    bitsToYield -= currentYieldNb;
    offsets[bufferPosition] = currentYieldNb;
    bufferPosition++;
    }

  *dest =
      (((((((*dest | buffers[0]) << offsets[1]) | buffers[1]) << offsets[2]) |
         buffers[2]) << offsets[3]) | buffers[3]);

  return offsets[0] + offsets[1] + offsets[2] + offsets[3];
}

bool end_of_bitstream(struct bitstream *stream) {
  return feof(stream->source);
}

/* Optionnel! */
void skip_bitstream_until(struct bitstream *stream,
                                        uint8_t byte) {
  // Si il existe un bit incomplet, on le saute.
  if(stream->posInByte != 0){
    if (fread(&stream->curByte, 1, 1, stream->source) != 1) {
      return;
    }
    stream->posInByte = 0;
  }
  while (stream->curByte != byte){

    if (fread(&stream->curByte, 1, 1, stream->source) != 1) {
      break;
    }

  }
  // On fait genre on a pas lu l'octet demandé si on l'a trouvé
  if(stream->curByte == byte){
    fseek(stream->source, ftell(stream->source) -1, SEEK_SET);

  }
}

bool read_bitstream_safe(struct bitstream *stream,
                              uint8_t nb_bits,
                              uint32_t *dest,
                              bool discard_byte_stuffing) {
    return read_bitstream(stream, nb_bits, dest, discard_byte_stuffing) == nb_bits;
}

void write_end_bitsream(struct bitstream *stream) {
    if (stream->posInByte) {
        fwrite(&stream->curByte, 1, 1, stream->source);
        stream->posInByte = 0;
        stream->curByte = 0;
    }
}

void write_bitstream(struct bitstream *stream, uint8_t nbBits, uint32_t toWrite) {
    if(!nbBits)
        return;

    while(nbBits) {
        uint8_t bit = toWrite >> (nbBits - 1) & 1;
        stream->curByte = (stream->curByte << 1) + bit;
        stream->posInByte++;

        if(stream->posInByte % BITS_IN_BYTES == 0) {
            fwrite(&stream->curByte, 1, 1, stream->source);

            if(stream->curByte == 0xFF) {
                stream->curByte = 0;
                fwrite(&stream->curByte, 1, 1, stream->source);
            }

            stream->curByte   = 0;
            stream->posInByte = 0;
        }

        nbBits--;
    }
}
