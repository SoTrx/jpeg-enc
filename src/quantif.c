
#include "quantif.h"
#include "JpegCst.h"
#include "blockFactory.h"

extern inline void dequantif(struct block16_t* orig, uint8_t* table){
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        orig->data[i] *= table[i];
    }
}

extern inline void quantif(struct block16_t* orig, uint8_t* table){
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        orig->data[i] /= table[i];
    }
}
