#include <stdarg.h>

#include "debug.h"

/**
 * Affiche le contenu d'un ublock8_t
 *
 * @param struct ublock8_t*  Le block à afficher
 */
void printf_struct_ublock8_t(struct ublock8_t* block) {
    for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            printf("%02x ", block->data[i + j * BLOCK_SIZE]);
        }
        printf("\n");
    }
}

/**
 * Affiche le contenu d'un block16_t
 *
 * @param struct block16_t*  Le block à afficher
 */
void printf_struct_block16_t(struct block16_t* block) {
    for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            printf("%04x ", (uint16_t)block->data[i + j * BLOCK_SIZE]);
        }
        printf("\n");
    }
}

void print_debug(bool isVerbose, char *format, ...) {
    if(isVerbose) {
        va_list list;
        va_start(list, format);
        vprintf(format, list);
        va_end(list);
    }
}

void exitIfTrue(bool condition, char *message, enum exitCode code) {
    if(condition) {
        printf("%s\n", message);
        exit(code);
    }
}
