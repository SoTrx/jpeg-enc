#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "acdc.h"
#include "bitstream.h"
#include "debug.h"
#include "huffman.h"

/**
 * Construit un flux de décodage JPEG
 *
 * @param struct huff_table** 		Le tableaux des tables de Huffman AC
 * @param struct huff_table** 		Le tableaux des tables de Huffman DC
 * @param uint8_t					Le nombre de composantes
 * @param uint8_t*					Les index réels dans le flux SOS
 * @param struct bitstream*			Le flux de lecture binaire
 * @param struct samplingFactors_t* Les facteurs d'échantillonnage
 * @return struct decodeStream_t* Le flux de décodage JPEG
 */
struct decodeStream_t* getDecodeStream(
	struct huff_table** tablesAC,
	struct huff_table** tablesDC,
	uint8_t nbComponents,
	uint8_t* real_indexes,
	struct bitstream* stream,
	struct samplingFactors_t* factors) {

	struct decodeStream_t* decode = malloc(sizeof(struct decodeStream_t));
	if(!decode){
		fatalError("Failed to allocate memory");
	}
	decode->tablesAC 	= tablesAC;
	decode->tablesDC 	= tablesDC;
	decode->real_indexes= real_indexes;
	decode->nbComponents= nbComponents;
	decode->factors 	= factors;
	decode->stream 		= stream;

	decode->lastDCY 	= 0;
	decode->lastDCCr 	= 0;
	decode->lastDCCb 	= 0;

	return decode;
}

/**
 * Retourne la valeur du coefficient codée par la magnitude et la
 * suite de bits qui suit cette magnitude
 *
 * @param struct bitstream*	 Le flux de données
 * @param uint8_t			 La magnitude du coefficient
 * @return int16_t 			 La valeur du coefficient
 */
static int16_t extractFromMagn(struct bitstream *stream, int8_t magn) {
	uint32_t index = 0;
	if (!read_bitstream_safe(stream, magn, &index, true)) {
		fatalError("Wrong number of bits red after magnitude");
	}

	return (index >= (1 << (magn - 1))) ? index : index - ((1 << magn) - 1);
}

/**
 * Lit la composante suivante dans le flot de données grâce au tables
 * de huffman AC et DC ainsi que le dernier coefficient DC
 *
 * @param int16_t* 				Le vecteur des données décodées de la composante
 * @param struct huffman_table* La table de huffman AC correspondant à la composante
 * @param struct huffman_table* Le table de huffman DC correspondant à la composante
 * @param struct bitstream* 	Le flux de données
 * @param uint16_t* 			Le dernier coefficient DC codé
 */
static void readComponent(int16_t* vector,
						  struct huff_table* ac,
						  struct huff_table *dc,
						  struct bitstream *stream,
						  int16_t *lastDC) {
	// Lecture de DC
	int8_t magn = next_huffman_value(dc, stream);
	int16_t DC 	= extractFromMagn(stream, magn);
	vector[0] 	= DC + (*lastDC);
	*lastDC 	= vector[0];

	// Lecture des AC suivants
	uint8_t AC;
	uint8_t currentIndex = 1;

	// Tant qu'on ne tombe pas sur un EOB
	while(currentIndex < COMPONENT_SIZE && (AC = next_huffman_value(ac, stream)) != EOB) {
		if (AC == ZRL) {
			currentIndex += 16;
		} else {
			// On étend les 0
			currentIndex += AC >> 4;

			// On écrit la magnitude décodée
			vector[currentIndex++] = extractFromMagn(stream, AC & 15);
		}
	}
	if (currentIndex > COMPONENT_SIZE) {
		fatalError("Too many AC values");
	}
}

/**
 * Réalise le décodage AC/DC à partir d'un flot de données de
 * décodage
 *
 * @param struct decodeStream_t* Le flux de décodage
 * @return struct blockMCU_t* Le MCU décodé
 */
struct blockMCU_t* decodeMCU(struct decodeStream_t* stream) {
    struct blockMCU_t *blockMCU = malloc(sizeof(struct blockMCU_t));
		if(!blockMCU){
			fatalError("Failed to allocate memory");
		}
    struct samplingFactors_t* factors = stream->factors;
    struct bitstream *bitstr = stream->stream;

    // Allocation du nombre de composantes nécessaires
    blockMCU->y  = calloc(factors->nbY, sizeof(struct block16_t));
    blockMCU->cb = (factors->nbCb) ? calloc(factors->nbCb, sizeof(struct block16_t)) : NULL;
    blockMCU->cr = (factors->nbCr) ? calloc(factors->nbCr, sizeof(struct block16_t)) : NULL;

	for (uint8_t k = 0; k < stream->nbComponents; k++) {
		if (stream->real_indexes[k] == 0) {
			// Lecture des composantes Y
		    for(uint8_t i = 0; i < factors->nbY; i++) {
		        readComponent(blockMCU->y[i].data, stream->tablesAC[0], stream->tablesDC[0], bitstr, &(stream->lastDCY));
		    }
		} else if (stream->real_indexes[k] == 1) {
			// Lecture des composantes Cb
		    for(uint8_t i = 0; i < factors->nbCb; i++) {
		        readComponent(blockMCU->cb[i].data, stream->tablesAC[1], stream->tablesDC[1], bitstr, &(stream->lastDCCb));
		    }
		} else if (stream->real_indexes[k] == 2) {
			// Lecture des composantes Cr
		    for(uint8_t i = 0; i < factors->nbCr; i++) {
		        readComponent(blockMCU->cr[i].data, stream->tablesAC[2], stream->tablesDC[2], bitstr, &(stream->lastDCCr));
		    }
		}
	}

    return blockMCU;
}

/**
 * Encode et écrit un block16_t dans le fichier.
 *
 * @param struct block16_t*				Le bloc à écrire
 * @param struct bitstream*				Le bitstream pour écrire
 * @param struct huff_table*			La table de Huffman pour DC
 * @param struct huff_table*			La table de Huffman pour AC
 * @param int16_t*						La valeur DC du bloc précédent
 */
void static encodeAndWriteBlock16(struct block16_t *block,
								  struct bitstream *stream,
								  struct huff_table *huff_table_dc,
								  struct huff_table *huff_table_ac,
							  	  int16_t *previous_dc_value) {

	/* Calcul et écriture de la valeur DC encodée avec Huffman */
	int16_t new_dc_value = block->data[0] - (*previous_dc_value);
	uint8_t magnitude = (new_dc_value == 0 ? 0 : (floor(log2(abs(new_dc_value))) + 1));
	uint32_t indice = (new_dc_value >= 0) ? (uint32_t)new_dc_value : (uint32_t)((int16_t)((1 << (magnitude)) - 1) + (int16_t)new_dc_value);

	if (magnitude > 11) {
		magnitude = 11;
		new_dc_value = (new_dc_value > 0 ? 0x07FF : 0xF801);
		indice = new_dc_value;
	}

	uint8_t code_length = 0;
	uint16_t huff_code = find_huffman_code(huff_table_dc,
								  		   magnitude,
								  		   &code_length);
	/* On écrit huff_code de longueur code_length */
	write_bitstream(stream, code_length, (uint32_t)huff_code);
	if (magnitude != 0) {
		/* On écrit indice de longueur magnitude */
		write_bitstream(stream, magnitude, indice);
	}

	/* Calcul et écriture des valeurs AC encodées RLE et Huffman */
	uint8_t nb_zero = 0;
	for (uint8_t i = 1 ; i < COMPONENT_SIZE; i++) {
		if (block->data[i] == 0) {
			if (nb_zero == 15) {
				/* On écrit 0xF0 encodé avec Huffman */
				huff_code = find_huffman_code(huff_table_ac, 0xF0, &code_length);
				write_bitstream(stream, code_length, (uint32_t)huff_code);
				nb_zero = 0;
			} else {
				nb_zero++;
			}
		} else {
			uint8_t rle = nb_zero << 4;
			magnitude = floor(log2(abs(block->data[i]))) + 1;
			rle |= magnitude;
			indice = (block->data[i] >= 0) ? (uint32_t)block->data[i] : (uint32_t)((int16_t)((1 << (magnitude)) - 1) + block->data[i]);

			huff_code = find_huffman_code(huff_table_ac, rle, &code_length);
			/* On écrit le symbole RLE (4bits nb_zero + 4bits magnitude) encodé Huffman de longueur code_length */
			write_bitstream(stream, code_length, (uint32_t)huff_code);
			/* On écrit indice de longueur magnitude */
			write_bitstream(stream, magnitude, indice);
			nb_zero = 0;
		}
	}
	if (nb_zero > 0) {
		/* On écrit le EOB 0x00 */
		huff_code = find_huffman_code(huff_table_ac, 0x00, &code_length);
		write_bitstream(stream, code_length, huff_code);
	}
	*previous_dc_value = block->data[0];
}

/**
 * Écrit les MCUs avec encodage AC/DC RLE/Huffman dans le fichier passé en
 * paramètre.
 *
 * @param struct blockMCU_t**		La liste de blocs à écrire
 * @param uint32_t					Le nombre de blocs MCU dans la liste
 * @param struct samplingFactors_t*	Les facteurs d'échantillonnage
 * @param struct bitstream*			Le bitstream pour écrire les données
 * @param struct huff_table**		Liste des tables de Huffman DC
 * @param struct huff_table**		Liste des tables de Huffman AC
 */
void encodeAndWriteAllMCU(struct blockMCU_t **blocksMCU,
						  uint32_t nb_mcu,
						  struct samplingFactors_t *factors,
						  struct bitstream *stream,
					  	  struct huff_table **huff_tables_dc,
						  struct huff_table **huff_tables_ac) {
	int16_t previous_dc_value_y = 0, previous_dc_value_cb = 0, previous_dc_value_cr = 0;
	for (uint32_t k = 0; k < nb_mcu; k++) {
		for (uint8_t i = 0; i < factors->nbY; i++) {
			encodeAndWriteBlock16(&blocksMCU[k]->y[i],
								  stream,
								  huff_tables_dc[0],
								  huff_tables_ac[0],
							  	  &previous_dc_value_y);
		}
		for (uint8_t i = 0; i < factors->nbCb; i++) {
			encodeAndWriteBlock16(&blocksMCU[k]->cb[i],
								  stream,
							  	  huff_tables_dc[1],
							  	  huff_tables_ac[1],
							  	  &previous_dc_value_cb);
		}
		for (uint8_t i = 0; i < factors->nbCr; i++) {
			encodeAndWriteBlock16(&blocksMCU[k]->cr[i],
								  stream,
							  	  huff_tables_dc[1],
							  	  huff_tables_ac[1],
							  	  &previous_dc_value_cr);
		}
	}
	write_end_bitsream(stream);
}
