#include <stdbool.h>
#include <stdlib.h>

#include "dzz.h"
#include "JpegCst.h"

static const uint8_t zz_tab[64] = {
	0, 1, 8, 16, 9, 2, 3, 10,
	17, 24, 32, 25, 18, 11, 4, 5,
	12, 19, 26, 33, 40, 48, 41, 34,
	27, 20, 13, 6, 7, 14, 21, 28,
	35, 42, 49, 56, 57, 50, 43, 36,
	29, 22, 15, 23, 30, 37, 44, 51,
	58, 59, 52, 45, 38, 31, 39, 46,
	53, 60, 61, 54, 47, 55, 62, 63
};

/**
 * Réalise l'étape de zig-zag à l'envers sur les données
 * d'un bloc.
 * Cette fonction est plus rapide que inverseZigZag.
 *
 * @param struct block16_t* Le vecteur/bloc à modifier
 */
void inverseZigZag(struct block16_t* vector) {
	int16_t new_vector[COMPONENT_SIZE];

	for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
		new_vector[zz_tab[i]] = vector->data[i];
	}
	for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
		vector->data[i] = new_vector[i];
	}
}

/**
 * Réalise l'étape de zig-zag sur les données d'un bloc.
 *
 * @param struct block16_t* Le vecteur/bloc à modifier
 */
void zigZag(struct block16_t* vector) {
	int16_t new_vector[COMPONENT_SIZE];

	for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
		new_vector[i] = vector->data[zz_tab[i]];
	}
	for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
		vector->data[i] = new_vector[i];
	}
}
