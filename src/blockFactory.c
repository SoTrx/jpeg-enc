#include "blockFactory.h"
#include "debug.h"


static inline void* smalloc(size_t dataSize){
    void * attr = malloc(dataSize);
    if (!attr) {
        fatalError("Failed to allocate memory");
    }
    return attr;
}

struct ublock8_t* make_ublock8(){
    struct ublock8_t* block = (struct ublock8_t*) smalloc(sizeof(struct ublock8_t));
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        block->data[i] = 0;
    }
    return block;
}

struct block16_t* make_block16(){
    struct block16_t* block = (struct block16_t*) smalloc(sizeof(struct block16_t));
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        block->data[i] = 0;
    }
    return block;
}

void free_ublock8(struct ublock8_t* block){
    if (block) {
        free(block);
        block = NULL;
    }
}

void free_block16(struct block16_t * block){
    if (block) {
        free(block);
        block = NULL;
    }
}

void free_nblock16(struct block16_t* blocks) {
    if (blocks) {
        free(blocks);
        blocks = NULL;
    }
}

struct colorBlock_t* make_colorBlock(bool isGrayScale){
    struct colorBlock_t* block = (struct colorBlock_t*) smalloc(sizeof(struct colorBlock_t));
    block->a = make_block16();
    if (!isGrayScale) {
        block->b = make_block16();
        block->c = make_block16();
    } else {
        block->b = NULL;
        block->c = NULL;
    }
    return block;
}

void free_colorBlock(struct colorBlock_t* block){
    if (block) {
        free_block16(block->a);
        free_block16(block->b);
        free_block16(block->c);
        free(block);
        block = NULL;
    }
}

void free_blockMCU(struct blockMCU_t* block) {
    if (block) {
        free_nblock16(block->y);
        free_nblock16(block->cb);
        free_nblock16(block->cr);
        free(block);
        block = NULL;
    }
}
