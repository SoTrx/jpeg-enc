#include "idct.h"

/**
 * Normalise la valeur donnée en paramètre pour donner un nombre
 * compris dans l'intervalle [0, 255]
 *
 * @param float La valeur à normaliser
 * @return uint8_t La valeur normalisée
 */
static inline uint8_t normalise_float_to_uint8(const float value) {
    return (value > 255) ? 255 : (value < 0) ? 0 : (uint8_t)roundf(value);
}

/**
 * Normalise la valeur donnée en paramètre pour donner un nombre
 * compris dans l'intervalle [0, 255]
 *
 * @param double La valeur à normaliser
 * @return uint8_t La valeur normalisée
 */
static inline uint8_t normalise_double_to_uint8(const double value) {
    return (value > 255) ? 255 : (value < 0) ? 0 : (uint8_t)roundf(value);
}

/**
 * Évalue la fonction C(x)
 *
 * @param float value La valeur à évaluer
 * @return float Le résultat de C(x)
 */
static inline float function_c(const uint8_t value) {
    return (value == 0) ? SQRT2INVERT : 1.0;
}

/**
 * Calcule la valeur spatiale via la transformée en cosinus discrète inverse
 *
 * @param struct block16_t* block_temporel Le block temporel d'origine
 * @param uint8_t x La coordonnée spatiale x du calcul
 * @param uint8_t y La coordonnée spatiale y du calcul
 * @return float La valeur de l'idct aux coordonnées spécifiées
 */
static float compute_idct_spatial_value(
    struct block16_t* block_temporel,
    uint8_t x,
    uint8_t y
) {

    float result = 0.0;

    result += (0.5 * ((float)block_temporel->data[0]));
    for (uint8_t mu = 1; mu < BLOCK_SIZE; mu++) {
        /* result += C(lambda)*C(mu)*cos((2x+1)*lambda*pi/(2n))*cos((2y+1)*mu*pi/(2n))*phi(lambda,mu) */
        result += (
            SQRT2INVERT * (
                cos(((y << 1) + 1)*(mu * PI)/BLOCK_SIZE2) * (
                    (float)block_temporel->data[mu * BLOCK_SIZE]
                )
            )
        );
    }

    for (uint8_t lambda = 1; lambda < BLOCK_SIZE; lambda++) {
        result += (
            SQRT2INVERT * (
                cos(((x << 1) + 1)*(lambda * PI)/BLOCK_SIZE2) * (
                        (float)block_temporel->data[lambda]
                )
            )
        );
        for (uint8_t mu = 1; mu < BLOCK_SIZE; mu++) {
            /* result += C(lambda)*C(mu)*cos((2x+1)*lambda*pi/(2n))*cos((2y+1)*mu*pi/(2n))*phi(lambda,mu) */
            result += (
                cos(((x << 1) + 1)*(lambda * PI)/BLOCK_SIZE2) * (
                    cos(((y << 1) + 1)*(mu * PI)/BLOCK_SIZE2) * (
                        (float)block_temporel->data[lambda + mu * BLOCK_SIZE]
                    )
                )
            );
        }
    }

    /* result = result / sqrt(2 * width) */
    result *= 0.25;
    /* Ajout de l'offset de 128 */
    result += 128.0;

    return result;
}

/**
 * Réalise la transformée de cosinus discret inverse sur le
 * bloc passé en paramètre
 *
 * @param struct block16_t* Le bloc en question
 */
void idct(struct block16_t* block) {
    uint8_t data[COMPONENT_SIZE];
    for (uint8_t y = 0; y < BLOCK_SIZE; y++) {
        for (uint8_t x = 0; x < BLOCK_SIZE; x++) {
            data[x + y * BLOCK_SIZE] = normalise_float_to_uint8(
                compute_idct_spatial_value(
                    block,
                    x,
                    y
                )
            );
        }
    }
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        block->data[i] = (int16_t)data[i];
    }
}

/**
 * Implémentation de l'algorithme de Loeffler en 1 dimension
 * http://www.ijaiem.org/volume3issue5/IJAIEM-2014-05-31-117.pdf
 * http://eejournal.ktu.lt/index.php/elt/article/viewFile/611/636
 *
 * @param double *data  Les données à traiter et à remplacer
 */
static void idct_loeffler_1d(double *data) {
    double temp_i[8], temp_o[8];

    /* Loeffler : "Stage 1" */
    temp_o[0] = data[0];
    temp_o[1] = data[4];
    temp_o[2] = data[2];
    temp_o[3] = data[6];
    temp_o[4] = (data[1] - data[7]);
    temp_o[5] = data[3] * SQRT2;
    temp_o[6] = data[5] * SQRT2;
    temp_o[7] = (data[1] + data[7]);

    /* Loeffler : "Stage 2" */
    temp_i[0] = (temp_o[0] + temp_o[1]);
    temp_i[1] = (temp_o[0] - temp_o[1]);
    temp_i[2] = temp_o[2] * LOEFFLER_CST1 - temp_o[3] * LOEFFLER_CST2;
    temp_i[3] = temp_o[3] * LOEFFLER_CST1 + temp_o[2] * LOEFFLER_CST2;
    temp_i[4] = (temp_o[4] + temp_o[6]);
    temp_i[5] = (temp_o[7] - temp_o[5]);
    temp_i[6] = (temp_o[4] - temp_o[6]);
    temp_i[7] = (temp_o[7] + temp_o[5]);

    /* Loeffler : "Stage 3" */
    temp_o[0] = (temp_i[0] + temp_i[3]);
    temp_o[1] = (temp_i[1] + temp_i[2]);
    temp_o[2] = (temp_i[1] - temp_i[2]);
    temp_o[3] = (temp_i[0] - temp_i[3]);
    temp_o[4] = temp_i[4] * LOEFFLER_CST3 - temp_i[7] * LOEFFLER_CST4;
    temp_o[5] = temp_i[5] * LOEFFLER_CST5 - temp_i[6] * LOEFFLER_CST6;
    temp_o[6] = temp_i[6] * LOEFFLER_CST5 + temp_i[5] * LOEFFLER_CST6;
    temp_o[7] = temp_i[7] * LOEFFLER_CST3 + temp_i[4] * LOEFFLER_CST4;

    /* Loeffler : "Stage 4" */
    data[0] = (temp_o[0] + temp_o[7]);
    data[1] = (temp_o[1] + temp_o[6]);
    data[2] = (temp_o[2] + temp_o[5]);
    data[3] = (temp_o[3] + temp_o[4]);
    data[4] = (temp_o[3] - temp_o[4]);
    data[5] = (temp_o[2] - temp_o[5]);
    data[6] = (temp_o[1] - temp_o[6]);
    data[7] = (temp_o[0] - temp_o[7]);
}

/**
 * Application de l'algorithme de Loeffler sur un block16_t (2D).
 * Pour cela on applique l'algorithme de Loeffler 1D sur chaque ligne
 * puis sur chaque colonne de la matrice. On obtient alors le résultat.
 *
 * @param struct block16_t* Le bloc en question
 */
void idct_loeffler(struct block16_t* block) {
    double input[BLOCK_SIZE];
    double matrix[COMPONENT_SIZE];

    /* Application de Loeffler sur les lignes */
    for (uint8_t k = 0; k < BLOCK_SIZE; k++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            input[i] = block->data[i + (k << 3)]; /* [i + k*BLOCK_SIZE] */
        }
        idct_loeffler_1d(input);
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            matrix[i + (k << 3)] = input[i]; /* [i + k*BLOCK_SIZE] */
        }
    }

    /* Application de Loeffler sur les colonnes */
    for (uint8_t k = 0; k < BLOCK_SIZE; k++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            input[i] = matrix[k + (i << 3)]; /* [k + i*BLOCK_SIZE] */
        }
        idct_loeffler_1d(input);
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            block->data[k + (i << 3)] = (int16_t)( /* [k + i*BLOCK_SIZE] */
                normalise_double_to_uint8(input[i]*0.125 + 128.0) /* input[i]/8 + 128 */
            );
        }
    }
}

/**
 * Application de l'algorithme de Loeffler sur un vecteur
 * http://www.ijaiem.org/volume3issue5/IJAIEM-2014-05-31-117.pdf
 * http://eejournal.ktu.lt/index.php/elt/article/viewFile/611/636
 *
 * @param float *data   Les données à traiter et à remplacer
 */
static void dct_loeffler_1d(double *data) {
    double temp_i[8], temp_o[8];

    /* Loeffler : "Stage 1" */
    temp_i[0] = data[0] + data[7];
    temp_i[1] = data[1] + data[6];
    temp_i[2] = data[2] + data[5];
    temp_i[3] = data[3] + data[4];
    temp_i[4] = data[3] - data[4];
    temp_i[5] = data[2] - data[5];
    temp_i[6] = data[1] - data[6];
    temp_i[7] = data[0] - data[7];

    /* Loeffler : "Stage 2" */
    temp_o[0] = temp_i[0] + temp_i[3];
    temp_o[1] = temp_i[1] + temp_i[2];
    temp_o[2] = temp_i[1] - temp_i[2];
    temp_o[3] = temp_i[0] - temp_i[3];
    temp_o[4] = temp_i[4] * LOEFFLER_CST3 + temp_i[7] * LOEFFLER_CST4;
    temp_o[5] = temp_i[5] * LOEFFLER_CST5 + temp_i[6] * LOEFFLER_CST6;
    temp_o[6] = temp_i[6] * LOEFFLER_CST5 - temp_i[5] * LOEFFLER_CST6;
    temp_o[7] = temp_i[7] * LOEFFLER_CST3 - temp_i[4] * LOEFFLER_CST4;

    /* Loeffler : "Stage 3" */
    temp_i[0] = temp_o[0] + temp_o[1];
    temp_i[1] = temp_o[0] - temp_o[1];
    temp_i[2] = temp_o[2] * LOEFFLER_CST1 + temp_o[3] * LOEFFLER_CST2;
    temp_i[3] = temp_o[3] * LOEFFLER_CST1 - temp_o[2] * LOEFFLER_CST2;
    temp_i[4] = temp_o[4] + temp_o[6];
    temp_i[5] = temp_o[7] - temp_o[5];
    temp_i[6] = temp_o[4] - temp_o[6];
    temp_i[7] = temp_o[7] + temp_o[5];

    /* Loeffler : "Stage 4" */
    data[0] = temp_i[0];
    data[4] = temp_i[1];
    data[2] = temp_i[2];
    data[6] = temp_i[3];
    data[7] = temp_i[7] - temp_i[4];
    data[3] = temp_i[5] * SQRT2;
    data[5] = temp_i[6] * SQRT2;
    data[1] = temp_i[7] + temp_i[4];
}

/**
 * Application de l'algorithme de Loeffler sur un ublock8_t (2D).
 * Pour cela on applique l'algorithme de Loeffler 1D sur chaque ligne
 * puis sur chaque colonne de la matrice. On obtient alors le résultat.
 *
 * @param struct block16_t* Le bloc en question
 */
void dct_loeffler(struct block16_t* block) {
    double input[BLOCK_SIZE];
    double matrix[COMPONENT_SIZE];

    /* Application de Loeffler sur les lignes */
    for (uint8_t k = 0; k < BLOCK_SIZE; k++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            /* (block_spatial->data[i + k * BLOCK_SIZE] - 128.0) / 8.0 */
            input[i] = ((double)block->data[i + (k << 3)] - 128.0) * 0.125;
        }
        dct_loeffler_1d(input);
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            matrix[i + (k << 3)] = input[i]; /* [i + k * BLOCK_SIZE] */
        }
    }

    /* Application de Loeffler sur les colonnes */
    for (uint8_t k = 0; k < BLOCK_SIZE; k++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            input[i] = matrix[k + (i << 3)]; /* [k + i * BLOCK_SIZE] */
        }
        dct_loeffler_1d(input);
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            block->data[k + (i << 3)] = (int16_t)round(input[i]);
        }
    }
}

/**
 * Calcule la valeur fréquentielle via la transformée en cosinus discrète
 *
 * @param struct ublock8_t* block_spatial Le block spatial d'origine
 * @param uint8_t i La coordonnée fréquentielle i du calcul
 * @param uint8_t j La coordonnée fréquentielle j du calcul
 * @return int16_t La valeur de l'dct aux coordonnées spécifiées
 */
static int16_t compute_dct_frequency_value(
    struct block16_t* block_spatial,
    uint8_t i,
    uint8_t j
) {
    float result = 0.0;

    for (uint8_t x = 0; x < BLOCK_SIZE; x++) {
        for (uint8_t y = 0; y < BLOCK_SIZE; y++) {
            /* result += cos((2x+1)*i*pi/(2n))*cos((2y+1)*j*pi/(2n))*pixel(x,y) */
            result += (
                cos(((x << 1) + 1)*(i * PI)/BLOCK_SIZE2) * (
                    cos(((y << 1) + 1)*(j * PI)/BLOCK_SIZE2) * (
                        (float)block_spatial->data[x + y * BLOCK_SIZE] - 128.0
                    )
                )
            );
        }
    }

    /* result = result * C(i) * C(j) */
    result *= function_c(i) * function_c(j);

    /* result = result / sqrt(2 * width) */
    result *= 0.25;

    return (int16_t)roundf(result);
}

/**
 * Réalise la transformée de cosinus discret sur le bloc
 * passé en paramètre
 *
 * @param struct block16_t* Le bloc en question
 */
void dct(struct block16_t* block) {
    int16_t data[COMPONENT_SIZE];
    for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            data[i + j * BLOCK_SIZE] = (
                compute_dct_frequency_value(
                    block,
                    i,
                    j
                )
            );
        }
    }
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        block->data[i] = data[i];
    }
}
