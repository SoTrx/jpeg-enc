#include "sampler.h"
#include "debug.h"

#define NB_VALIDS 21

static const struct samplingFactors_t validFactors[NB_VALIDS] = {
    {1, 1, 1, 0, 0, 0, 0, 0, 0},
    {1, 1, 1, 1, 1, 1, 1, 1, 1},
    {2, 1, 2, 1, 1, 1, 1, 1, 1},
    {1, 2, 2, 1, 1, 1, 1, 1, 1},
    {2, 1, 2, 2, 1, 2, 2, 1, 2},
    {1, 2, 2, 1, 2, 2, 1, 2, 2},
    {2, 2, 4, 1, 1, 1, 1, 1, 1},
    {2, 2, 4, 1, 2, 2, 1, 2, 2},
    {2, 2, 4, 2, 1, 2, 2, 1, 2},
    {2, 2, 4, 2, 1, 2, 1, 2, 2},
    {2, 2, 4, 1, 2, 2, 2, 1, 2},
    {2, 2, 4, 1, 1, 1, 1, 2, 2},
    {2, 2, 4, 2, 1, 2, 1, 1, 1},
    {2, 2, 4, 1, 2, 2, 1, 1, 1},
    {2, 2, 4, 1, 1, 1, 2, 1, 2},
    {2, 2, 4, 2, 2, 4, 1, 1, 1},
    {2, 2, 4, 1, 1, 1, 2, 2, 4},
    {4, 1, 4, 2, 1, 2, 1, 1, 1},
    {1, 4, 4, 1, 2, 2, 1, 1, 1},
    {4, 2, 8, 1, 1, 1, 1, 1, 1},
    {2, 4, 8, 1, 1, 1, 1, 1, 1}
};

/**
 * Vérifie si les facteurs d'échantillonnage passés en paramètres sont
 * supportés par le logiciel de décodage actuel
 *
 * @param struct samplingFactors_t*     Facteurs d'échantillonnage
 * @return bool                         True si supportés
 */
bool checkSamplingFactors(struct samplingFactors_t* factors) {
    for (uint8_t i = 0; i < NB_VALIDS; i++) {
        if (factors->hY != validFactors[i].hY) {
            continue;
        } else if (factors->vY != validFactors[i].vY) {
            continue;
        } else if (factors->nbY != validFactors[i].nbY) {
            continue;
        } else if (factors->hCb != validFactors[i].hCb) {
            continue;
        } else if (factors->vCb != validFactors[i].vCb) {
            continue;
        } else if (factors->nbCb != validFactors[i].nbCb) {
            continue;
        } else if (factors->hCr != validFactors[i].hCr) {
            continue;
        } else if (factors->vCr != validFactors[i].vCr) {
            continue;
        } else if (factors->nbCr != validFactors[i].nbCr) {
            continue;
        } else {
            return true;
        }
    }
    return false;
}

/**
 * Retourne true si aucun sous-échantillonnage n'est présent
 * @param struct samplingFactors_t* Les facteurs à vérifier
 * @return bool Le résultat du test
 */
static inline bool isWithoutSubSampling(struct samplingFactors_t* factors) {
    return (
        factors->hY == factors->hCb && factors->hY == factors->hCr &&
        factors->vY == factors->vCb && factors->vY == factors->vCr
    ) || (
        factors->hCb == 0 || factors->hCr == 0 /* Image sans composantes Cb, Cr */
    );
}

/**
 * Retourne true si il y a un sous-échantillonnage horizontal
 * @param struct samplingFactors_t* Les facteurs à vérifier
 * @return bool Le résultat du test
 */
static inline bool isWithHorizontalSampling(struct samplingFactors_t* factors) {
    return (
        factors->hY == (factors->hCb << 1) && factors->hY == (factors->hCr << 1)
    );
}

/**
 * Retourne true si il y a un sous-échantillonnage vertical
 * @param struct samplingFactors_t* Les facteurs à vérifier
 * @return bool Le résultat du test
 */
static inline bool isWithVerticalSampling(struct samplingFactors_t* factors) {
    return (
        factors->vY == (factors->vCb << 1) && factors->vY == (factors->vCr << 1)
    );
}

/**
 * Sur-échantillonne un bloc à partir des paramètres de
 * sous-échantillonnage
 *
 * @param struct blockMCU_t* Le bloc à sur-échantillonner
 * @param struct samplingFactors_t* Les facteurs d'échantillonnage
 */
void sampler_upSample(
    struct blockMCU_t* block,
    struct samplingFactors_t* factors
) {
    /**
     * Si on est dans le cas d'une image non sous-échantillonnée
     * on retourne directement le MCU
     */
    if (isWithoutSubSampling(factors)) {
        return;
    }

    /**
     * On conserve toujours la composante Y et on doit sur-échantillonner Cb et Cr
     */

    struct block16_t *cb = calloc(factors->nbY, sizeof(struct block16_t));
    struct block16_t *cr = calloc(factors->nbY, sizeof(struct block16_t));

    uint8_t index, newIndexCb, newIndexCr;
    uint8_t offsetVCb, offsetVCr, offsetHCb, offsetHCr;
    uint8_t currentIndex = 0, chromeIndexCb, chromeIndexCr;

    // On sur-échantillonne sur Cb et Cr selon le facteur de sous-échantillonnage
    for(uint8_t v = 0; v < factors->vY; v++) {
        offsetVCb = ((factors->vY / factors->vCb) > 1) ? (uint8_t)(v * (BLOCK_SIZE / (factors->vY / factors->vCb)) * BLOCK_SIZE) : 0;
        offsetVCr = ((factors->vY / factors->vCr) > 1) ? (uint8_t)(v * (BLOCK_SIZE / (factors->vY / factors->vCr)) * BLOCK_SIZE) : 0;

        for(uint8_t h = 0; h < factors->hY; h++) {
            offsetHCb = ((factors->hY / factors->hCb) > 1) ? (uint8_t)(h * (BLOCK_SIZE / (factors->hY / factors->hCb))) : 0;
            offsetHCr = ((factors->hY / factors->hCr) > 1) ? (uint8_t)(h * (BLOCK_SIZE / (factors->hY / factors->hCr))) : 0;

            for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
                for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
                    index = (uint8_t)(i * BLOCK_SIZE + j);
                    newIndexCb = (uint8_t)((i / (factors->vY / factors->vCb)) * BLOCK_SIZE + (j / (factors->hY / factors->hCb)) + offsetHCb + offsetVCb);
                    newIndexCr = (uint8_t)((i / (factors->vY / factors->vCr)) * BLOCK_SIZE + (j / (factors->hY / factors->hCr)) + offsetHCr + offsetVCr);

                    chromeIndexCb = (factors->hCb > 1) ? currentIndex % factors->hCb : ((factors->vCb == factors->hCb) ? 0 : currentIndex / factors->vCb);
                    chromeIndexCr = (factors->hCr > 1) ? currentIndex % factors->hCr : ((factors->vCr == factors->hCr) ? 0 : currentIndex / factors->vCr);

                    cb[currentIndex].data[index] = block->cb[chromeIndexCb].data[newIndexCb];
                    cr[currentIndex].data[index] = block->cr[chromeIndexCr].data[newIndexCr];
                }
            }
            currentIndex++;
        }
    }

    free_nblock16(block->cb);
    free_nblock16(block->cr);

    block->cb = cb;
    block->cr = cr;
}

/**
 * Fabrique un bloc de couleur
 * @param struct block16_t* Composante 1 de la couleur
 * @param struct block16_t* Composante 2 de la couleur
 * @param struct block16_t* Composante 3 de la couleur
 * @return struct colorBlock_t* Le bloc de couleur fabriqué
 */
static struct colorBlock_t *createYCbCr(struct block16_t *a, struct block16_t *b, struct block16_t *c) {
    struct colorBlock_t *color = malloc(sizeof(struct colorBlock_t));
    color->a = a;
    color->b = b;
    color->c = c;
    return color;
}

/**
 * Transforme un ensemble de MCU en un ensemble de blocks YCbCr utilisables.
 *
 * @param struct colorBlock_t**     Liste de blocs YCbCr ordonnée en sortie
 * @param struct blockMCU_t**       La liste de blocs MCU
 * @param uint32_t                  Le nombre de blocs MCU dans la liste
 * @param struct samplingFactors_t* Les facteurs d'échantillonnage
 * @param uint8_t                   Le nombre de composantes dans l'image
 * @param uint16_t                  Le nombre de blocks 8x8 sur la largeur de l'image
 */
extern void sampler_MCUsubdivide(
    struct colorBlock_t** blocks_ycbcr,
    struct blockMCU_t** blocks_mcu,
    uint32_t nb_mcu,
    struct samplingFactors_t* factors,
    uint8_t nb_components,
    uint16_t nb_blocks_per_line
) {
    //       |\__/,|   (`\
    //     _.|o o  |_   ) )
    // ---(((---(((---------
    uint32_t offsetX, offsetY;
    uint8_t currentIndex;

    uint32_t realBlockPerLine   = nb_blocks_per_line / factors->hY;
    uint32_t realLineCount      = nb_blocks_per_line * (factors->vY - 1);

    if(nb_components == 3) {
        for (uint32_t i = 0; i < nb_mcu; i++) {
            currentIndex = 0;
            // Décalage en X des blocs dû au sous-échantillonnage horizontal
            offsetX = i * factors->hY;
            // Décalage en Y des blocs dû au sous-échantillonnage vertical
            offsetY = (i / realBlockPerLine) * realLineCount;

            // Pour chaque MCU on sur-échantillonne
            for (uint8_t v = 0; v < factors->vY; v++) {
                for (uint8_t h = 0; h < factors->hY; h++) {
                    blocks_ycbcr[offsetX + h + offsetY + v * nb_blocks_per_line] = createYCbCr(
                            &blocks_mcu[i]->y[currentIndex],
                            &blocks_mcu[i]->cb[currentIndex],
                            &blocks_mcu[i]->cr[currentIndex]);
                    currentIndex++;
                }
            }
        }
    } else {
        for (uint32_t i = 0; i < nb_mcu; i++) {
            blocks_ycbcr[i] = createYCbCr(&blocks_mcu[i]->y[0], NULL, NULL);
        }
    }
}

extern void subsample(
    struct blockMCU_t* block,
    struct samplingFactors_t* factors
){
 // Si on ne demande aucun sous-echantillonage, on va pas cherher plus loin.
  if(isWithoutSubSampling(factors)){
    return;
  }
  struct block16_t *cb = NULL;
  struct block16_t *cr = NULL;
  if(isWithHorizontalSampling(factors) && !isWithVerticalSampling(factors)){
    cb = calloc(factors->hY * factors->vY / 2, sizeof(struct block16_t));
    cr = calloc(factors->hY * factors->vY / 2, sizeof(struct block16_t));
    uint8_t countMatrix = 0;
    for (uint8_t j = 0; j < BLOCK_SIZE; j+=2) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
            uint8_t toFuseIndex1 = i + j * BLOCK_SIZE;
            uint8_t toFuseIndex2 = i + (j +1) *BLOCK_SIZE;
            //printf("Indexes %d %d\n", toFuseIndex1, toFuseIndex2);
            cb[0].data[countMatrix] = (block->cb[0].data[toFuseIndex1] + block->cb[0].data[toFuseIndex2])/2;
            cb[1].data[countMatrix] = (block->cb[1].data[toFuseIndex1] + block->cb[1].data[toFuseIndex2])/2;
            cr[0].data[countMatrix] = (block->cr[0].data[toFuseIndex1] + block->cr[0].data[toFuseIndex2])/2;
            cr[1].data[countMatrix] = (block->cr[1].data[toFuseIndex1] + block->cr[1].data[toFuseIndex2])/2;
            countMatrix++;
        }
    }
  }else if(isWithVerticalSampling(factors) && ! isWithHorizontalSampling(factors)){

    cb = calloc(factors->hY * factors->vY / 2, sizeof(struct block16_t));
    cr = calloc(factors->hY * factors->vY / 2, sizeof(struct block16_t));
    uint8_t countMatrix = 0;
    for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i+=2) {
            uint8_t toFuseIndex1 = i + j*BLOCK_SIZE;
            uint8_t toFuseIndex2 = i + 1 + j*BLOCK_SIZE;

            cb[0].data[countMatrix] = (block->cb[0].data[toFuseIndex1] + block->cb[0].data[toFuseIndex2])/2;
            cb[1].data[countMatrix] = (block->cb[1].data[toFuseIndex1] + block->cb[1].data[toFuseIndex2])/2;
            cr[0].data[countMatrix] = (block->cr[0].data[toFuseIndex1] + block->cr[0].data[toFuseIndex2])/2;
            cr[1].data[countMatrix] = (block->cr[1].data[toFuseIndex1] + block->cr[1].data[toFuseIndex2])/2;

            countMatrix++;
        }
    }
  }else if(isWithVerticalSampling(factors) && isWithHorizontalSampling(factors)){
    cb = calloc(factors->hY * factors->vY / 4, sizeof(struct block16_t));
    cr = calloc(factors->hY * factors->vY / 4, sizeof(struct block16_t));
    uint8_t countMatrix = 0;
    for (uint8_t j = 0; j < BLOCK_SIZE; j+=2) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i+=2) {
            uint8_t toFuseIndex1 = i + j * BLOCK_SIZE;
            uint8_t toFuseIndex2 = i + (j +1) *BLOCK_SIZE;
            uint8_t toFuseIndex3 = i + j*BLOCK_SIZE;
            uint8_t toFuseIndex4 = i + 1 + j*BLOCK_SIZE;
            cb[0].data[countMatrix] = (block->cb[0].data[toFuseIndex1] + block->cb[0].data[toFuseIndex2] + block->cb[0].data[toFuseIndex3] + block->cb[0].data[toFuseIndex4])/4;
            cb[1].data[countMatrix] = (block->cb[1].data[toFuseIndex1] + block->cb[1].data[toFuseIndex2] + block->cb[1].data[toFuseIndex3] + block->cb[1].data[toFuseIndex4])/4;
            cb[2].data[countMatrix] = (block->cb[2].data[toFuseIndex1] + block->cb[2].data[toFuseIndex2] + block->cb[2].data[toFuseIndex3] + block->cb[2].data[toFuseIndex4])/4;
            cb[3].data[countMatrix] = (block->cb[3].data[toFuseIndex1] + block->cb[3].data[toFuseIndex2] + block->cb[3].data[toFuseIndex3] + block->cb[3].data[toFuseIndex4])/4;
            cr[0].data[countMatrix] = (block->cb[0].data[toFuseIndex1] + block->cb[0].data[toFuseIndex2] + block->cb[0].data[toFuseIndex3] + block->cb[0].data[toFuseIndex4])/4;
            cr[1].data[countMatrix] = (block->cb[1].data[toFuseIndex1] + block->cb[1].data[toFuseIndex2] + block->cb[1].data[toFuseIndex3] + block->cb[1].data[toFuseIndex4])/4;
            cr[2].data[countMatrix] = (block->cb[2].data[toFuseIndex1] + block->cb[2].data[toFuseIndex2] + block->cb[2].data[toFuseIndex3] + block->cb[2].data[toFuseIndex4])/4;
            cr[3].data[countMatrix] = (block->cb[3].data[toFuseIndex1] + block->cb[3].data[toFuseIndex2] + block->cb[3].data[toFuseIndex3] + block->cb[3].data[toFuseIndex4])/4;
            countMatrix++;
        }
    }
  }
  else{
    fatalError("Bad sampling factors");
  }

  free_nblock16(block->cb);
  free_nblock16(block->cr);

  block->cb = cb;
  block->cr = cr;
}
