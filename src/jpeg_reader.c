#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "jpeg_reader.h"
#include "huffman.h"
#include "JpegCst.h"
#include "debug.h"

struct jpeg_desc {
    char filename[100];
    struct bitstream *stream;
    uint8_t nb_quantization_tables;
    uint8_t *quantization_tables[4];
    uint8_t nb_huffman_tables_ac;
    struct huff_table *huffman_tables_ac[4];
    uint8_t nb_huffman_tables_dc;
    struct huff_table *huffman_tables_dc[4];
    uint16_t image_width;
    uint16_t image_height;
    uint8_t nb_components;
    uint8_t frame_components_id[3];
    uint8_t frame_components_sampling_factors[3];
    uint8_t frame_components_quant_indexes[3];
    uint8_t scan_components_id[3];
    uint8_t scan_components_huffman_indexes_ac[3];
    uint8_t scan_components_huffman_indexes_dc[3];
    uint8_t precision;
};

static uint32_t read_huffman_table(struct jpeg_desc *jpeg) {
    uint32_t bits_dest;

    if (!read_bitstream_safe(jpeg->stream, 3, &bits_dest, false) || bits_dest != 0) {
        fatalError("DHT section abrupt ending. File is corrupted. Aborting");
    }

    if (!read_bitstream_safe(jpeg->stream, 1, &bits_dest, false)){
      fatalError("DHT table type mismatch. Aborting");
    }

    enum acdc current_type = bits_dest;

    if (current_type == AC) {
        jpeg->nb_huffman_tables_ac++;
    } else {
        jpeg->nb_huffman_tables_dc++;
    }

    if (!read_bitstream_safe(jpeg->stream, 4, &bits_dest, false) || bits_dest > 3) {
        fatalError("Current Huffman table is incomplete. Aborting");
    }

    uint16_t nb_byte_read, current_indice = bits_dest;

    struct huff_table *table = load_huffman_table(jpeg->stream, &nb_byte_read);

    if (table == NULL || nb_byte_read <= 0) {
      fatalError("Huffman table could not be read. Aborting");
    }

    if (current_type == AC) {
        jpeg->huffman_tables_ac[current_indice] = table;
    } else {
        jpeg->huffman_tables_dc[current_indice] = table;
    }

    return nb_byte_read + 8;
}

/**
 * Retourne une structure jpeg_desc
 */
struct jpeg_desc *read_jpeg(const char *filename) {
    struct jpeg_desc *jpeg = malloc(sizeof(struct jpeg_desc));
    if(!jpeg){
      fatalError("Failed to allocate memory");
    }

    if (strlen(filename) >= 100) {
        fatalError("File name is too long !");
    }

    strcpy(jpeg->filename, filename);
    jpeg->stream = create_bitstream(filename);
    jpeg->nb_quantization_tables = 0;
    jpeg->nb_huffman_tables_ac = 0;
    jpeg->nb_huffman_tables_dc = 0;
    jpeg->image_width = 0;
    jpeg->image_height = 0;
    jpeg->nb_components = 0;
    jpeg->precision = 0;

    uint32_t bits_dest, section_size;
    bool APP0_found = false, DHT_found = false, DQT_found = false, SOF0_found = false;

    /* On vérifie qu'il s'agit d'une image JPEG */
    if (!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false) || bits_dest != SOI) {
        fatalError("Unknwon file type.");

    }

    while (bits_dest != SOS) {
        skip_bitstream_until(jpeg->stream, 0xFF);

        if (!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false) || end_of_bitstream(jpeg->stream)) {
            /* Fin du fichier atteint */
            fatalError("No SOS section found. Aborting");
        } else if (bits_dest == APP0) {
            /**
             * Section APP0
             *
             * 2 octets : longueur de la section
             * 5 octets : doit être égal à "JFIF\0"
             * x octets : données non prises en compte
             */
            if(!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false)) {
                fatalError("APP0 length mismatch. Aborting");
            }

            /* On vérifie qu'il s'agit bien d'un fichier au format JFIF */
            if (!read_bitstream_safe(jpeg->stream, 32, &bits_dest, false) || bits_dest != 0x4A464946) {
                fatalError("Image format isn't JFIF complying. Aborting");
            }

            APP0_found = true;
        } else if (bits_dest == DHT) {
            /**
             * Section DHT
             *
             * 2 octets : longueur de la section
             * 3 bits : obligatoirement nulls
             * 1 bit : type de la table (0=DC, 1=AC)
             * 4 bits : indice de la table (compris entre 0 et 3)
             * x octets : début de la table de Huffman
             */
            if (!read_bitstream_safe(jpeg->stream, 16, &section_size, false)) {
                fatalError("DHT length mismatch. Aborting");
            }
            uint32_t section_size_read = 2;
            do {
                section_size_read += read_huffman_table(jpeg);
            } while (section_size_read < section_size);

            DHT_found = true;
        } else if (bits_dest == DQT) {
            /**
             * Section DQT
             *
             * 2 octets : longueur de la section
             * 4 bits : précision (0=8bits, 1=16bits)
             * 4 bits : indice iQ de la table de quantification
             * 64 octets : valeurs de la table de quantification
             */
            uint8_t nb_tables = 0, indice;
            if (!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false)) {
                fatalError("DQT length mismatch");
            }
            nb_tables = (bits_dest - 3) / 64;

            for (uint8_t i = 0; i < nb_tables; i++) {
                if (!read_bitstream_safe(jpeg->stream, 4, &bits_dest, false)) {
                    fatalError("DQT table precision error. Aborting");

                }
                jpeg->precision = bits_dest;

                if (!read_bitstream_safe(jpeg->stream, 4, &bits_dest, false)) {
                  fatalError("DQT table index error. Aborting");
                }
                indice = bits_dest;

                if (indice >= 4) {
                  fatalError("DQT table index mismatch. Aborting");
                }

                /* Lecture de la table DQT de quantification */
                uint8_t *table_dqt = calloc(64, sizeof(uint8_t));
                for (uint8_t j = 0; j < 64; j++) {
                    if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
                        fatalError("DQT content error");
                    }
                    table_dqt[j] = bits_dest & 0xFF;
                }

                jpeg->nb_quantization_tables++;
                jpeg->quantization_tables[indice] = table_dqt;
            }

            DQT_found = true;
        } else if (bits_dest == SOF0) {
            /**
             * Section SOF
             *
             * 2 octets : longueur de la section
             * 1 octet : précision en bits par composante (ici 8)
             * 2 octets : hauteur en pixels de l'image
             * 2 octets : largeur en pixels de l'image
             * 1 octet : nombre de composantes N
             * N fois :
             *      1 octet : identifiant iC de composante (0...255)
             *      4 bits : facteur d'échantillonnage horizontal (1...4)
             *      4 bits : facteur d'échantillonnage vertical (1...4)
             *      1 octet : table de quantification iQ associée (0...3)
             */
            if (!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false)) {
                fatalError("SOF0 length could not be read. Aborting");
            }
            if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
                fatalError("SOF0 precision could not be read");
            }
            jpeg->precision = bits_dest & 0xFF;

            if (jpeg->precision != 8) {
                fatalError(" Precision mismatch. Not compatible with JFIF format");
            }

            if (!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false)) {
                fatalError("SOF0 height mismatch");
            }
            jpeg->image_height = bits_dest & 0xFFFF;
            if (!read_bitstream_safe(jpeg->stream, 16, &bits_dest, false)) {
                fatalError("SOF0 width mismatch");
            }
            jpeg->image_width = bits_dest & 0xFFFF;

            if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
                fatalError("SOF0 components number error");
            }
            jpeg->nb_components = bits_dest & 0xFF;

            if (jpeg->nb_components != 3 && jpeg->nb_components != 1) {
                fatalError("Components number not supported");
            }

            uint8_t component_id = 0;

            for (uint8_t i = 0; i < jpeg->nb_components; i++) {
                if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
                    fatalError("Could not obtain frame component id");
                }
                component_id = bits_dest & 0xFF;
                jpeg->frame_components_id[i] = component_id;

                if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
                    fatalError("Could not obtain frame component sampling factors");
                }
                jpeg->frame_components_sampling_factors[i] = bits_dest & 0xFF;

                if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
                    fatalError("Could not obtain DQT table index in SOF0");
                }

                if (bits_dest >= 4) {
                    fatalError("DQT table with component index error");
                }

                jpeg->frame_components_quant_indexes[i] = bits_dest & 0xFF;
            }

            SOF0_found = true;
        }
    }

    if (!APP0_found) {
        fatalError(" No section APP0 found");

    } else if (!DHT_found) {
        fatalError(" No section DHT found");

    } else if (!DQT_found) {
        fatalError(" No section DQT found");

    } else if (!SOF0_found) {
        fatalError(" No section SOF0 found");

    }

    /* Section SOS */
    if (!read_bitstream_safe(jpeg->stream, 16, &section_size, false)) {
        fatalError("SOS section length mismatch");

    }
    if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
        fatalError("SOS component error");

    }
    jpeg->nb_components = bits_dest & 0xFF;

    if (jpeg->nb_components != 3 && jpeg->nb_components != 1) {
        fatalError(" Component number unsupported");

    }

    uint8_t scan_component_id = 0;

    for (uint8_t i = 0; i < jpeg->nb_components; i++) {
        if (!read_bitstream_safe(jpeg->stream, 8, &bits_dest, false)) {
            fatalError("Could not read component id SOS");

        }
        scan_component_id = bits_dest & 0xFF;
        jpeg->scan_components_id[i] = scan_component_id;
        if (!read_bitstream_safe(jpeg->stream, 4, &bits_dest, false)) {
            fatalError("Could not read SOS' huffman DC index");

        }
        jpeg->scan_components_huffman_indexes_dc[i] = bits_dest & 0xF;
        if (!read_bitstream_safe(jpeg->stream, 4, &bits_dest, false)) {
            fatalError("Could not read SOS' huffman AC index");

        }
        jpeg->scan_components_huffman_indexes_ac[i] = bits_dest & 0xF;
    }

    /* On positionne le bitstream juste après l'en-tête de SOS */
    if (!read_bitstream_safe(jpeg->stream, 24, &bits_dest, false)) {
        fatalError("cursor position mismatch on SOS raw data.");
   }

    return jpeg;
}

void close_jpeg(struct jpeg_desc *jpeg) {
    close_bitstream(jpeg->stream);

    for (uint8_t i = 0; i < jpeg->nb_quantization_tables; i++) {
        free(jpeg->quantization_tables[i]);
    }
    for (uint8_t i = 0; i < jpeg->nb_huffman_tables_ac; i++) {
        free_huffman_table(jpeg->huffman_tables_ac[i]);
    }
    for (uint8_t i = 0; i < jpeg->nb_huffman_tables_dc; i++) {
        free_huffman_table(jpeg->huffman_tables_dc[i]);
    }

    free(jpeg);
}

/**
 * Retourne un bitstream qui pointe directement sur les données brutes
 */
struct bitstream *get_bitstream(const struct jpeg_desc *jpeg) {
    return jpeg->stream;
}

/**
 * Retourne le nombre de tables de quantification dans le fichier
 */
uint8_t get_nb_quantization_tables(const struct jpeg_desc *jpeg) {
    return jpeg->nb_quantization_tables;
}

/**
 * Retourne une table de quantification
 */
uint8_t *get_quantization_table(const struct jpeg_desc *jpeg, uint8_t index) {
    if (index >= jpeg->nb_quantization_tables) {
        fatalError("DQT index mismatch");
    }
    return jpeg->quantization_tables[index];
}

/**
 * Retourne le nombre de tables de Huffman
 */
uint8_t get_nb_huffman_tables(const struct jpeg_desc *jpeg, enum acdc type) {
    if (type == AC) {
        return jpeg->nb_huffman_tables_ac;
    } else if (type == DC) {
        return jpeg->nb_huffman_tables_dc;
    } else {
        fatalError("Huffman table type mismatch");
    }
}

/**
 * Retourne une table de Huffman
 */
struct huff_table *get_huffman_table(const struct jpeg_desc *jpeg,
                                    enum acdc type,
                                    uint8_t index) {
    if (type == AC) {
        if (index >= jpeg->nb_huffman_tables_ac) {
            fatalError("Index mismatch in current huffman table");
        }
        return jpeg->huffman_tables_ac[index];
    } else if (type == DC) {
        if (index >= jpeg->nb_huffman_tables_dc) {
            fatalError("Index mismatch in current huffman table");
        }
        return jpeg->huffman_tables_dc[index];
    } else {
        fatalError("Huffman table type mismatch");
    }
}

/**
 * Retourne la taille de l'image suivant la direction spécifiée.
 */
uint16_t get_image_size(struct jpeg_desc *jpeg, enum direction dir) {
    if (dir == DIR_H) {
        return jpeg->image_width;
    } else if (dir == DIR_V) {
        return jpeg->image_height;
    } else {
        fatalError("Direction demandée invalide\n");
    }
}

uint8_t get_nb_components(const struct jpeg_desc *jpeg) {
    return jpeg->nb_components;
}

uint8_t get_frame_component_id(const struct jpeg_desc *jpeg,
                                uint8_t frame_comp_index) {
    if (frame_comp_index >= jpeg->nb_components) {
        fatalError("Frame component index is greater than nb components\n");
    }
    return jpeg->frame_components_id[frame_comp_index];
}

uint8_t get_frame_component_sampling_factor(const struct jpeg_desc *jpeg,
                                            enum direction dir,
                                            uint8_t frame_comp_index) {
    if (frame_comp_index >= jpeg->nb_components) {
        fatalError(" frame component index is greater than nb components\n");
    }
    if (dir == DIR_H) {
        return (jpeg->frame_components_sampling_factors[frame_comp_index] >> 4) & 0xF;
    } else if (dir == DIR_V) {
        return jpeg->frame_components_sampling_factors[frame_comp_index] & 0xF;
    } else {
        fatalError("Incorrect direction");
    }
}

uint8_t get_frame_component_quant_index(const struct jpeg_desc *jpeg,
                                        uint8_t frame_comp_index) {
    if (frame_comp_index >= jpeg->nb_components) {
        fatalError("Frame component index is greater than nb components\n");
    }
    return jpeg->frame_components_quant_indexes[frame_comp_index];
}

uint8_t get_scan_component_id(const struct jpeg_desc *jpeg,
                                uint8_t scan_comp_index) {
    return jpeg->scan_components_id[scan_comp_index];
}

uint8_t get_scan_component_huffman_index(const struct jpeg_desc *jpeg,
                                            enum acdc type,
                                            uint8_t scan_comp_index) {
    if (type == AC) {
        return jpeg->scan_components_huffman_indexes_ac[scan_comp_index];
    } else if (type == DC) {
        return jpeg->scan_components_huffman_indexes_dc[scan_comp_index];
    } else {
        fatalError("Huffman table type mismatch");
    }
}
