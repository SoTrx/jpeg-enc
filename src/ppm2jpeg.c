/**
 * Programme principal d'encodage JPEG à partir d'un PPM
 */
#define _POSIX_C_SOURCE 2

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "debug.h"
#include "argParse.h"
#include "acdc.h"
#include "huffman.h"
#include "jpeg_reader.h"
#include "bitstream.h"
#include "idct.h"
#include "color_converter.h"
#include "quantif.h"
#include "dzz.h"
#include "ppm.h"
#include "sampler.h"


int main(int argc, char **argv)
{
    bool isVerbose = false;
    // OUI C'EST STANDARD #include <unistd.h>
    int opt;
    while ((opt = getopt(argc, argv, "vh")) != -1) {
        switch (opt) {
            case 'v': isVerbose = true; break;
            case 'h' :
                fprintf(stderr, "Usage: %s [-v] FILE \n", argv[0]);
                sanic();
                exit(EXIT_SUCCESS);
                break;
            default:
                fprintf(stderr, "Usage: %s [-v] FILE \n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }
    if (optind >= argc) {
        fprintf(stderr, "No file supplied\n");
        exit(EXIT_FAILURE);
    }
    /* On recupere le nom du fichier JPEG sur la ligne de commande. Pour obtenir le descripteur */
    if (!strstr(argv[optind], ".ppm") && !strstr(argv[optind], ".pgm")) {
        fprintf(stderr, "%s is not a jpeg | jpg | JPG file or do not exists\n", argv[optind]);
        exit(EXIT_FAILURE);
    }

    /* On recupere le nom du fichier PPM sur la ligne de commande. */
    const char* filename = argv[optind];

    /* Le facteur de qualité doit être compris en 1 et 100 */
    uint32_t quality_factor = 95;
    /* On crée les tables de quantification correspondantes */
    float quality_factor_s = (
        (quality_factor < 50) ? (5000 / quality_factor) : (200 - 2 * quality_factor));

    uint8_t quantization_table_y[COMPONENT_SIZE] = {
        16, 11, 10, 16, 24, 40, 51, 61,
        12, 12, 14, 19, 26, 58, 60, 55,
        14, 13, 16, 24, 40, 57, 69, 56,
        14, 17, 22, 29, 51, 87, 80, 62,
        18, 22, 37, 56, 68, 109, 103, 77,
        24, 35, 55, 64, 81, 104, 113, 92,
        49, 64, 78, 87, 103, 121, 120, 101,
        72, 92, 95, 98, 112, 100, 103, 99
    };
    uint8_t quantization_table_c[COMPONENT_SIZE] = {
        17, 18, 24, 47, 99, 99, 99, 99,
        18, 21, 26, 66, 99, 99, 99, 99,
        24, 26, 56, 99, 99, 99, 99, 99,
        47, 66, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99,
        99, 99, 99, 99, 99, 99, 99, 99
    };

    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        quantization_table_y[i] = floor((quality_factor_s * quantization_table_y[i] + 50) / 100.0);
        quantization_table_c[i] = floor((quality_factor_s * quantization_table_c[i] + 50) / 100.0);
    }

    uint8_t *quantization_tables[2] = {quantization_table_y, quantization_table_c};

    /* Récupération des blocs de l'image */
    struct colorBlock_t **colorBlocks;
    uint16_t nb_blocks_per_line, nb_blocks_per_column, image_size_horizontal, image_size_vertical;
    bool isGrayScale = true;
    readPPM(filename,
            &colorBlocks,
            &nb_blocks_per_line,
            &nb_blocks_per_column,
            &image_size_horizontal,
            &image_size_vertical,
            &isGrayScale
    );

    print_debug(isVerbose, "Taille de l'image : %u x %u\n", image_size_horizontal, image_size_vertical);

    struct samplingFactors_t sampling_factors = {1, 1, 1, 0, 0, 0, 0, 0, 0};

    if (!isGrayScale) {
        sampling_factors.hCb = 1;
        sampling_factors.vCb = 1;
        sampling_factors.nbCb = 1;
        sampling_factors.hCr = 1;
        sampling_factors.vCr = 1;
        sampling_factors.nbCr = 1;
    }

    uint32_t nb_blockmcu = ((uint32_t)(ceil(image_size_horizontal / (8.0 * sampling_factors.hY)) *
                                       ceil(image_size_vertical   / (8.0 * sampling_factors.vY))));
    uint32_t nb_block8 = nb_blockmcu * sampling_factors.hY * sampling_factors.vY;
    nb_blocks_per_line = (uint16_t)ceil(image_size_horizontal / (8.0*sampling_factors.hY)) * sampling_factors.hY;

    print_debug(isVerbose, "%u MCUs à traiter\n", nb_blockmcu);
    print_debug(isVerbose, "%u Blocks 8x8 à traiter\n", nb_block8);

    exitIfTrue(nb_blockmcu == 0, "Nombre de blocks à traiter insuffisant", TOO_FEW_BLOCKS);

    uint8_t nb_components = 1;

    if (!isGrayScale) {
        nb_components = 3;
    }

    /* On convertit maintenant l'image de blocks RGB en blocks YCbCr */
    if (nb_components == 3) {
        for (uint32_t i = 0; i < nb_block8; i++) {
            convertRGBToYCbCr(colorBlocks[i]);
        }
    }

    struct blockMCU_t *blocksMCU[nb_blockmcu];
    for (uint32_t i = 0; i < nb_blockmcu; i++) {
        blocksMCU[i] = malloc(sizeof(struct blockMCU_t));
        if(!blocksMCU[i]){
          fatalError("Failed to allocate memory");
        }
        blocksMCU[i]->y = colorBlocks[i]->a;
        blocksMCU[i]->cb = colorBlocks[i]->b;
        blocksMCU[i]->cr = colorBlocks[i]->c;
    }

    for (uint32_t i = 0; i < nb_blockmcu; i++) {
        for(uint8_t j = 0; j < sampling_factors.nbY; j++) {
            dct_loeffler(&blocksMCU[i]->y[j]);
            quantif(&blocksMCU[i]->y[j], quantization_tables[0]);
            zigZag(&blocksMCU[i]->y[j]);
        }

        for(uint8_t j = 0; j < sampling_factors.nbCb; j++) {
            dct_loeffler(&blocksMCU[i]->cb[j]);
            quantif(&blocksMCU[i]->cb[j], quantization_tables[1]);
            zigZag(&blocksMCU[i]->cb[j]);
        }

        for(uint8_t j = 0; j < sampling_factors.nbCr; j++) {
            dct_loeffler(&blocksMCU[i]->cr[j]);
            quantif(&blocksMCU[i]->cr[j], quantization_tables[1]);
            zigZag(&blocksMCU[i]->cr[j]);
        }
    }

    /* Définition des tables de Huffman classiques */
    uint8_t huffman_table_dc_y[28] = {
        0x00, 0x01, 0x05, 0x01, 0x01, 0x01, 0x01, 0x01,
        0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B
    };
    uint8_t huffman_table_dc_c[28] = {
        0x00, 0x03, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
        0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B
    };
    uint8_t huffman_table_ac_y[178] = {
        0x00, 0x02, 0x01, 0x03, 0x03, 0x02, 0x04, 0x03,
        0x05, 0x05, 0x04, 0x04, 0x00, 0x00, 0x01, 0x7D,
        0x01, 0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12,
        0x21, 0x31, 0x41, 0x06, 0x13, 0x51, 0x61, 0x07,
        0x22, 0x71, 0x14, 0x32, 0x81, 0x91, 0xA1, 0x08,
        0x23, 0x42, 0xB1, 0xC1, 0x15, 0x52, 0xD1, 0xF0,
        0x24, 0x33, 0x62, 0x72, 0x82, 0x09, 0x0A, 0x16,
        0x17, 0x18, 0x19, 0x1A, 0x25, 0x26, 0x27, 0x28,
        0x29, 0x2A, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
        0x3A, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49,
        0x4A, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59,
        0x5A, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69,
        0x6A, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79,
        0x7A, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
        0x8A, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98,
        0x99, 0x9A, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7,
        0xA8, 0xA9, 0xAA, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6,
        0xB7, 0xB8, 0xB9, 0xBA, 0xC2, 0xC3, 0xC4, 0xC5,
        0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xD2, 0xD3, 0xD4,
        0xD5, 0xD6, 0xD7, 0xD8, 0xD9, 0xDA, 0xE1, 0xE2,
        0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA,
        0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8,
        0xF9, 0xFA
    };
    uint8_t huffman_table_ac_c[178] = {
        0x00, 0x02, 0x01, 0x02, 0x04, 0x04, 0x03, 0x04,
        0x07, 0x05, 0x04, 0x04, 0x00, 0x01, 0x02, 0x77,
        0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21,
        0x31, 0x06, 0x12, 0x41, 0x51, 0x07, 0x61, 0x71,
        0x13, 0x22, 0x32, 0x81, 0x08, 0x14, 0x42, 0x91,
        0xA1, 0xB1, 0xC1, 0x09, 0x23, 0x33, 0x52, 0xF0,
        0x15, 0x62, 0x72, 0xD1, 0x0A, 0x16, 0x24, 0x34,
        0xE1, 0x25, 0xF1, 0x17, 0x18, 0x19, 0x1A, 0x26,
        0x27, 0x28, 0x29, 0x2A, 0x35, 0x36, 0x37, 0x38,
        0x39, 0x3A, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48,
        0x49, 0x4A, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58,
        0x59, 0x5A, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
        0x69, 0x6A, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
        0x79, 0x7A, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
        0x88, 0x89, 0x8A, 0x92, 0x93, 0x94, 0x95, 0x96,
        0x97, 0x98, 0x99, 0x9A, 0xA2, 0xA3, 0xA4, 0xA5,
        0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xB2, 0xB3, 0xB4,
        0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xC2, 0xC3,
        0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xD2,
        0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8, 0xD9, 0xDA,
        0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9,
        0xEA, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8,
        0xF9, 0xFA
    };

    uint16_t nb_byte_read = 0;
    struct huff_table *huff_tables_dc[2];
    huff_tables_dc[0] = load_huffman_table_memory(huffman_table_dc_y, &nb_byte_read);
    huff_tables_dc[1] = load_huffman_table_memory(huffman_table_dc_c, &nb_byte_read);
    struct huff_table *huff_tables_ac[2];
    huff_tables_ac[0] = load_huffman_table_memory(huffman_table_ac_y, &nb_byte_read);
    huff_tables_ac[1] = load_huffman_table_memory(huffman_table_ac_c, &nb_byte_read);

    /* On détermine le nom du fichier de sortie en modifiant l'extension */
    char new_filename[strlen(filename) + 3];
    strcpy(new_filename, filename);
    char *extension = strstr(new_filename, ".p");

    exitIfTrue(extension == NULL, "Le nom du fichier ne contient pas d'extension .ppm ou .pgm", EXTENSION_ERROR);

    strcpy(extension, "2.jpg\0");

    /* Écriture du JPEG */
    FILE *fichier = NULL;
    fichier = fopen(new_filename, "wb");

    if (fichier == NULL) {
        printf("Fichier de sortie non accessible\n");
        return EXIT_FAILURE;
    }

    /* Marqueur SOI */
    char soi_string[2] = {0xFF, 0xD8};
    fwrite(soi_string, sizeof(char), 2, fichier);

    /* Section APP0 */
    char app_string[18] = {0xFF, 0xE0, 0x00, 0x10, 'J', 'F', 'I', 'F', '\0',
                          0x01, 0x01, 0x01, 0x00, 0x48, 0x00, 0x48, 0x00, 0x00};
    fwrite(app_string, sizeof(char), 18, fichier);

    /* Sections DQT */
    /* On applique ZigZag sur les tables de quantifications */
    struct block16_t quant_table_y, quant_table_c;
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        quant_table_y.data[i] = quantization_table_y[i];
        quant_table_c.data[i] = quantization_table_c[i];
    }
    zigZag(&quant_table_y);
    zigZag(&quant_table_c);
    uint8_t quant_table_y_8[COMPONENT_SIZE], quant_table_c_8[COMPONENT_SIZE];
    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        quant_table_y_8[i] = quant_table_y.data[i];
        quant_table_c_8[i] = quant_table_c.data[i];
    }

    char dqt_string[5] = {0xFF, 0xDB, 0x00, 0x43, 0x00};
    fwrite(dqt_string, sizeof(char), 5, fichier);
    fwrite(quant_table_y_8, sizeof(uint8_t), 64, fichier);

    if (nb_components == 3) {
        dqt_string[4] = 0x01;
        fwrite(dqt_string, sizeof(char), 5, fichier);
        fwrite(quant_table_c_8, sizeof(uint8_t), 64, fichier);
    }

    /* Section SOF0 */
    uint8_t sof_length = 8 + 3 * nb_components;
    char sof_string[5] = {0xFF, 0xC0, 0x00, sof_length, 0x08};
    fwrite(sof_string, sizeof(char), 5, fichier);
    uint8_t size_vertical[2] = {image_size_vertical >> 8, image_size_vertical};
    fwrite(&size_vertical, sizeof(uint8_t), 2, fichier);
    uint8_t size_horizontal[2] = {image_size_horizontal >> 8, image_size_horizontal};
    fwrite(&size_horizontal, sizeof(uint8_t), 2, fichier);
    fwrite(&nb_components, sizeof(uint8_t), 1, fichier);

    uint8_t sampling = (sampling_factors.vY << 4) | sampling_factors.hY;
    char sof_compo_string[3] = {0x01, sampling, 0x00};
    fwrite(sof_compo_string, sizeof(char), 3, fichier);

    if (nb_components == 3) {
        sampling = (sampling_factors.vCb << 4) | sampling_factors.hCb;
        char sof_compo_string_2[3] = {0x02, sampling, 0x01};
        fwrite(sof_compo_string_2, sizeof(char), 3, fichier);
        sampling = (sampling_factors.vCr << 4) | sampling_factors.hCr;
        char sof_compo_string_3[3] = {0x03, sampling, 0x01};
        fwrite(sof_compo_string_3, sizeof(char), 3, fichier);
    }

    /* Sections DHT */
    char dht_string[5] = {0xFF, 0xC4, 0x00, 0x1F, 0x00};
    fwrite(dht_string, sizeof(char), 5, fichier);
    fwrite(huffman_table_dc_y, sizeof(uint8_t), 28, fichier);

    if (nb_components == 3) {
        char dht_string_2[5] = {0xFF, 0xC4, 0x00, 0x1F, 0x01};
        fwrite(dht_string_2, sizeof(char), 5, fichier);
        fwrite(huffman_table_dc_c, sizeof(uint8_t), 28, fichier);
    }

    char dht_string_3[5] = {0xFF, 0xC4, 0x00, 0xB5, 0x10};
    fwrite(dht_string_3, sizeof(char), 5, fichier);
    fwrite(huffman_table_ac_y, sizeof(uint8_t), 178, fichier);

    if (nb_components == 3) {
        char dht_string_4[5] = {0xFF, 0xC4, 0x00, 0xB5, 0x11};
        fwrite(dht_string_4, sizeof(char), 5, fichier);
        fwrite(huffman_table_ac_c, sizeof(uint8_t), 178, fichier);
    }

    /* Section SOS */
    uint8_t sos_length = 6 + 2 * nb_components;
    char sos_string[5] = {0xFF, 0xDA, 0x00, sos_length, nb_components};
    fwrite(sos_string, sizeof(char), 5, fichier);

    char sos_coeff[2] = {0x01, 0x00};
    fwrite(sos_coeff, sizeof(char), 2, fichier);

    if (nb_components == 3) {
        sos_coeff[0] = 0x02;
        sos_coeff[1] = 0x11;
        fwrite(sos_coeff, sizeof(char), 2, fichier);
        sos_coeff[0] = 0x03;
        sos_coeff[1] = 0x11;
        fwrite(sos_coeff, sizeof(char), 2, fichier);
    }

    char sos_unused[3] = {0x00, 0x3f, 0x00};
    fwrite(sos_unused, sizeof(char), 3, fichier);

    struct bitstream *stream = create_write_bitstream(fichier);

    /* Écriture des MCU encodés avec AC/DC */
    encodeAndWriteAllMCU(blocksMCU,
                         nb_blockmcu,
                         &sampling_factors,
                         stream,
                         huff_tables_dc,
                         huff_tables_ac);

    /* Marqueur EOI */
    char eoi_string[2] = {0xFF, 0xD9};
    fwrite(eoi_string, sizeof(char), 2, fichier);

    close_bitstream(stream);

    /* Libération des structures allouées */
    free_huffman_table(huff_tables_dc[0]);
    free_huffman_table(huff_tables_dc[1]);
    free_huffman_table(huff_tables_ac[0]);
    free_huffman_table(huff_tables_ac[1]);

    for (uint32_t i = 0; i < nb_blockmcu; i++) {
        free_blockMCU(blocksMCU[i]);
    }

    for (uint32_t i = 0; i < nb_block8; i++) {
        free(colorBlocks[i]);
    }

    free(colorBlocks);

    return EXIT_SUCCESS;
}
