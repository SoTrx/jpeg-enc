#include "color_converter.h"
#include "blockFactory.h"


/**
 * Normalise la valeur donnée en paramètre pour donner un nombre
 * compris dans l'intervalle [0, 255]
 *
 * @param float La valeur à normaliser
 * @return uint8_t La valeur normalisée
 */
static inline uint8_t normalise_float_to_uint8(const float value) {
    if (value > 255.0) {
        return 255;
    }
    else if (value < 0.0) {
        return 0;
    }
    else {
        return (uint8_t)round(value);
    }
}

/**
 * Convertit le bloc initialement en YCbCr en RGB
 *
 * @param struct blockBlock_t* Le bloc YCbCr initial
 */
void convertYCbCrToRGB(struct colorBlock_t* block_ycbcr) {
    float cb_128, cr_128, y;

    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        y = (float)block_ycbcr->a->data[i];
        cb_128 = (float)block_ycbcr->b->data[i] - 128.0;
        cr_128 = (float)block_ycbcr->c->data[i] - 128.0;

        block_ycbcr->a->data[i] = normalise_float_to_uint8(
            y - 0.0009267 * cb_128 + 1.4016868 * cr_128
        );
        block_ycbcr->b->data[i] = normalise_float_to_uint8(
            y - 0.3436954 * cb_128 - 0.7141690 * cr_128
        );
        block_ycbcr->c->data[i] = normalise_float_to_uint8(
            y + 1.7721604 * cb_128 + 0.0009902 * cr_128
        );
    }
}

/**
 * Convertit le bloc initialement en RGB en YCbCr
 *
 * @param struct colorBlock_t* Le bloc RGB initial
 */
void convertRGBToYCbCr(struct colorBlock_t* block_rgb) {
    float a, b, c;

    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        a = (float)block_rgb->a->data[i];
        b = (float)block_rgb->b->data[i];
        c = (float)block_rgb->c->data[i];
        block_rgb->a->data[i] = normalise_float_to_uint8(
            0.299 * a + 0.587 * b + 0.114 * c
        );
        block_rgb->b->data[i] = normalise_float_to_uint8(
            128.0 - 0.168736 * a - 0.331264 * b + 0.5 * c
        );
        block_rgb->c->data[i] = normalise_float_to_uint8(
            128.0 + 0.5 * a - 0.418688 * b - 0.081312 * c
        );
    }
}
