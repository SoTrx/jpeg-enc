/**
 * Programme principal de décodage JPEG vers PPM
 */
#define _POSIX_C_SOURCE 2

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "debug.h"
#include "argParse.h"
#include "acdc.h"
#include "huffman.h"
#include "jpeg_reader.h"
#include "bitstream.h"
#include "idct.h"
#include "color_converter.h"
#include "quantif.h"
#include "dzz.h"
#include "ppm.h"
#include "sampler.h"
/**
 * Récupère les index réels des composantes dans la section SOS
 * @param struct jpeg_desc* Descripteur du fichier JPEG
 * @param uint8_t**         La liste des index
 * @param uint8_t           Le nombre de composantes
 */
static void getRealIndexes(struct jpeg_desc *jdesc,
                           uint8_t *real_indexes,
                           uint8_t nb_components) {
    for (uint8_t i = 0; i < nb_components; i++) {
        uint8_t frame_component_id = get_frame_component_id(jdesc, i);
        for (uint8_t j = 0; j < nb_components; j++) {
            if (frame_component_id == get_scan_component_id(jdesc, j)) {
                real_indexes[i] = j;
                break;
            }
        }
    }
}

/**
 * Récupère les tables de quantification
 * @param struct jpeg_desc* Descripteur du fichier JPEG
 * @param uint8_t**         Le conteneurs des tables
 * @param uint8_t           Le nombre de composantes
 */
static void getQuantizationTables(struct jpeg_desc *jdesc,
                                  uint8_t **tables,
                                  const uint8_t nbComponents) {
    for (uint8_t i = 0; i < nbComponents; i++) {
        tables[i] = get_quantization_table(jdesc, get_frame_component_quant_index(jdesc, i));
    }
}

/**
 * Récupère les tables Huffman
 * @param struct jpeg_desc*     Descripteur du fichier JPEG
 * @param struct huff_table**   Le conteneurs des tables AC
 * @param struct huff_table **  Le conteneurs des tables DC
 * @param uint8_t               Le nombre de composantes
 */
static void getHuffmanTables(struct jpeg_desc *jdesc,
                             struct huff_table **ac,
                             struct huff_table **dc,
                             const uint8_t nbComponents) {
    for (uint8_t i = 0; i < nbComponents; i++) {
        ac[i] = get_huffman_table(jdesc, AC, get_scan_component_huffman_index(jdesc, AC, i));
    }

    for (uint8_t i = 0; i < nbComponents; i++) {
        dc[i] = get_huffman_table(jdesc, DC, get_scan_component_huffman_index(jdesc, DC, i));
    }
}

/**
 * Récupère les facteurs de sous-échantillonnage
 * @param struct jpeg_desc*             Descripteur du fichier JPEG
 * @param struct gengetopt_args_info*   Les arguments passés au programme
 * @param uint8_t                       Le nombre de composantes
 * @param struct samplingFactors_t*     Le conteneur des facteurs de sous-echantillonnage
 */
static void getSamplingFactors(struct jpeg_desc *jdesc,
                               bool isVerbose,
                               const uint8_t nbComponents,
                               struct samplingFactors_t *factors) {
    uint8_t h_scale_factors[nbComponents];
    uint8_t v_scale_factors[nbComponents];

    for (uint8_t i = 0; i < nbComponents; i++) {
        h_scale_factors[i] = get_frame_component_sampling_factor(jdesc, DIR_H, i);
        v_scale_factors[i] = get_frame_component_sampling_factor(jdesc, DIR_V, i);
        print_debug(isVerbose, "Composante %u : %u x %u\n", i, h_scale_factors[i], v_scale_factors[i]);
    }

    factors->hY = h_scale_factors[0];
    factors->vY = v_scale_factors[0];
    factors->nbY = factors->hY * factors->vY;

    if (nbComponents == 3) {
        factors->hCb = h_scale_factors[1];
        factors->vCb = v_scale_factors[1];
        factors->nbCb = factors->hCb * factors->vCb;
        factors->hCr = h_scale_factors[2];
        factors->vCr = v_scale_factors[2];
        factors->nbCr = factors->hCr * factors->vCr;
    } else {
        factors->hCb = 0;
        factors->vCb = 0;
        factors->nbCb = 0;
        factors->hCr = 0;
        factors->vCr = 0;
        factors->nbCr = 0;
    }

    uint8_t somme = factors->nbY + factors->nbCb + factors->nbCr;
    exitIfTrue(somme > 10, "Facteurs d'échantillonnages trop grands", SAMPLING_ERROR);

    exitIfTrue(!checkSamplingFactors(factors),
               "Facteurs d'échantillonnages non pris en charge",
               SAMPLING_ERROR);
}

int main(int argc, char **argv)
{

    bool isVerbose = false;
    // OUI C'EST STANDARD #include <unistd.h>
    int opt;
    while ((opt = getopt(argc, argv, "vh")) != -1) {
      switch (opt) {
      case 'v': isVerbose = true; break;
      case 'h' :
      fprintf(stderr, "Usage: %s [-v] FILE \n", argv[0]);
      sanic();
      exit(EXIT_SUCCESS);
      break;
      default:
          fprintf(stderr, "Usage: %s [-v] FILE \n", argv[0]);
          exit(EXIT_FAILURE);
      }
    }
    if(optind >= argc){
      fatalError("No file supplied");
    }
    /* On recupere le nom du fichier JPEG sur la ligne de commande. Pour obtenir le descripteur */
    if( !strstr(argv[optind], ".jpg") && !strstr(argv[optind], ".jpeg" ) && !strstr(argv[optind], ".JPG")){
      char message[100];
      sprintf(message, "%s is not a jpeg | jpg | JPG file or do not exists\n", argv[optind]);
      fatalError(message);
    }
    const char* filename = argv[optind];
    struct jpeg_desc *jdesc = read_jpeg(filename);

    uint8_t nb_components = get_nb_components(jdesc);

    /* Récupération des index réels correspondant au placement des YCbCr dans SOS */
    uint8_t real_indexes[nb_components];
    getRealIndexes(jdesc, real_indexes, nb_components);

    /* Récupération des tables de quantification */
    uint8_t nb_quantization_tables = get_nb_quantization_tables(jdesc);
    print_debug(isVerbose, "%u tables de quantification\n", nb_quantization_tables);

    uint8_t *quantization_tables[nb_components];
    getQuantizationTables(jdesc, quantization_tables, nb_components);

    /* On récupère le nombre de tables de Huffman pour AC */
    uint8_t nb_huffman_tables_ac = get_nb_huffman_tables(jdesc, AC);
    uint8_t nb_huffman_tables_dc = get_nb_huffman_tables(jdesc, DC);

    print_debug(isVerbose, "%u tables de Huffman pour AC\n", nb_huffman_tables_ac);
    print_debug(isVerbose, "%u tables de Huffman pour DC\n", nb_huffman_tables_dc);

    struct huff_table* huffman_tables_ac[nb_components];
    struct huff_table* huffman_tables_dc[nb_components];
    getHuffmanTables(jdesc, huffman_tables_ac, huffman_tables_dc, nb_components);

    /* Récupération de la taille de l'image */
    uint16_t image_size_horizontal = get_image_size(jdesc, DIR_H);
    uint16_t image_size_vertical   = get_image_size(jdesc, DIR_V);

    print_debug(isVerbose, "Taille de l'image : %u x %u\n", image_size_horizontal, image_size_vertical);
    exitIfTrue(image_size_vertical * image_size_horizontal < COMPONENT_SIZE, "Taille d'image trop faible", TOO_SMALL);

    /* Récupération des facteurs d'échantillonnage sur chaque composantes */
    print_debug(isVerbose, (nb_components == 1) ? "Image en niveaux de gris\n" : "Image en couleurs YCbCr\n");
    exitIfTrue(nb_components != 3 && nb_components != 1, "Colorimétrie de l'image non supportée", UNSUPPORTED_COLOR);
    print_debug(isVerbose, "Facteurs d'échantillonnage :\n");

    struct samplingFactors_t sampling_factors;
    getSamplingFactors(jdesc, isVerbose, nb_components, &sampling_factors);

    /* On recupere le flux des donnees brutes a partir du descripteur */
    struct bitstream *stream = get_bitstream(jdesc);

    struct decodeStream_t* decode_stream = getDecodeStream(
        huffman_tables_ac,
        huffman_tables_dc,
        nb_components,
        real_indexes,
        stream,
        &sampling_factors
    );

    uint32_t nb_blockmcu = ((uint32_t)(ceil(image_size_horizontal / (8.0 * sampling_factors.hY)) *
                                       ceil(image_size_vertical   / (8.0 * sampling_factors.vY))));
    uint32_t nb_block8 = nb_blockmcu * sampling_factors.hY * sampling_factors.vY;
    uint16_t nb_blocks_per_line = (uint16_t)ceil(image_size_horizontal / (8.0*sampling_factors.hY)) * sampling_factors.hY;

    print_debug(isVerbose, "%u MCUs à traiter\n", nb_blockmcu);
    print_debug(isVerbose, "%u Blocks 8x8 à traiter\n", nb_block8);

    exitIfTrue(nb_blockmcu == 0, "Nombre de blocks à traiter insuffisant", TOO_FEW_BLOCKS);

    struct blockMCU_t* blocksMCU[nb_blockmcu];

    /* Décompression des données */
    for (uint32_t i = 0; i < nb_blockmcu; i++) {
        blocksMCU[i] = decodeMCU(decode_stream);

        for(uint8_t j = 0; j < sampling_factors.nbY; j++) {
            dequantif(&blocksMCU[i]->y[j], quantization_tables[0]);
            inverseZigZag(&blocksMCU[i]->y[j]);
            idct_loeffler(&blocksMCU[i]->y[j]);
        }

        for(uint8_t j = 0; j < sampling_factors.nbCb; j++) {
            dequantif(&blocksMCU[i]->cb[j], quantization_tables[1]);
            inverseZigZag(&blocksMCU[i]->cb[j]);
            idct_loeffler(&blocksMCU[i]->cb[j]);
        }

        for(uint8_t j = 0; j < sampling_factors.nbCr; j++) {
            dequantif(&blocksMCU[i]->cr[j], quantization_tables[1]);
            inverseZigZag(&blocksMCU[i]->cr[j]);
            idct_loeffler(&blocksMCU[i]->cr[j]);
        }

        sampler_upSample(blocksMCU[i], &sampling_factors);
    }

    /* Reconstruction de l'image */
    struct colorBlock_t *colorBlocks[nb_block8];
    sampler_MCUsubdivide(colorBlocks, blocksMCU, nb_blockmcu, &sampling_factors, nb_components, nb_blocks_per_line);

    /* On convertit maintenant l'image de blocks YCbCr en blocks RGB */
    if (nb_components == 3) {
        for (uint32_t i = 0; i < nb_block8; i++) {
            convertYCbCrToRGB(colorBlocks[i]);
        }
    }

    /* On détermine le nom du fichier de sortie en modifiant l'extension */
    char new_filename[strlen(filename) + 2];
    strcpy(new_filename, filename);
    char *extension = strstr(new_filename, ".jp");

    exitIfTrue(extension == NULL, "Le nom du fichier ne contient pas d'extension .jpg ou .jpeg", EXTENSION_ERROR);

    strcpy(extension, (nb_components == 1) ? ".pgm\0" : ".ppm\0");

    /* Écriture dans le fichier final au format PPM ou PGM */
    writePPM(new_filename, colorBlocks, image_size_vertical, image_size_horizontal, nb_blocks_per_line, BINARY);

    /* LIBERER, DELIVRER */
    close_jpeg(jdesc);
    free(decode_stream);

    for (uint32_t i = 0; i < nb_blockmcu; i++) {
        free_blockMCU(blocksMCU[i]);
    }

    for (uint32_t i = 0; i < nb_block8; i++) {
        free(colorBlocks[i]);
    }


    return EXIT_SUCCESS;
}
