#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "JpegCst.h"
#include "blockFactory.h"
#include "ppm.h"
#include "debug.h"

/**
 * True if the image is a grayscale image
 * @param  Bloc to check
 * @return        Boolean
 */
static inline bool isGrayScale(const struct colorBlock_t *block) {
  return !block->b && !block->c && block->a != NULL;
}

/**
 * Returns a 32 bits representation of an RGB pixel given a block and a pixel
 * location.
 * @param  block Input buffer
 * @param  x     Horizontal offset
 * @param  y     Vertical offset
 * @return       32 bit int =>  rrggbb00
 * @see blockRGB_t
 */
static inline int32_t getRGBPixelAt(const struct colorBlock_t *block,
                                    const uint32_t index) {
  return (int32_t)0 | ((int32_t)block->c->data[index]) << 8 |
         ((int32_t)block->b->data[index]) << 16 |
         ((int32_t)block->a->data[index]) << 24;
}

/**
 * Returns a 32 bits representation of a Gray pixel given a block and a pixel
 * location.
 * @param  block Input buffer
 * @param  x     Horizontal offset
 * @param  y     Vertical offset
 * @return       32 bit int =>  0(32)|gg
 * @see blockRGB_t
 */
static inline int32_t getGrayPixelAt(struct colorBlock_t *block,
                                     const uint32_t index) {
  return (int32_t)block->a->data[index]; /* [y * BLOCK_SIZE + x] */
}

static inline int32_t getCorrespondingPixel(struct colorBlock_t **listBlocks,
                                            const uint32_t indexX,
                                            const uint32_t indexY,
                                            const uint16_t blocksPerLine,
                                            const bool isGrayScale) {
  const uint32_t numXBlock = indexX >> 3; /* indexX / BLOCK_SIZE */
  const uint32_t numYBlock = indexY >> 3; /* indexY / BLOCK_SIZE */
  const uint32_t index =
      ((indexY - (numYBlock << 3)) << 3) + indexX -
      (numXBlock
       << 3); /* (indexY % BLOCK_SIZE) * BLOCK_SIZE + (indexX % BLOCK_SIZE) */

  return isGrayScale
             ? getGrayPixelAt(listBlocks[blocksPerLine * numYBlock + numXBlock],
                              index)
             : getRGBPixelAt(listBlocks[blocksPerLine * numYBlock + numXBlock],
                             index);
}

void writePPM(const char *filename, struct colorBlock_t **list,
              const uint16_t imageHeight, const uint16_t imageWidth,
              const uint16_t blocksPerLine, const WriteMode mode) {
  assert(list != NULL);

  // Fichier
  FILE *outFile = fopen(filename, "wb");
  if (outFile == NULL) {
    fatalError(strerror(errno));
  }

  if (isGrayScale(list[0])) {
    if (mode == BINARY) {

      // headers
      fprintf(outFile, "%s\n%d %d\n%d\n", "P5", imageWidth, imageHeight, 255);

      for (uint32_t indexY = 0; indexY < imageHeight; indexY++) {
        for (uint32_t indexX = 0; indexX < imageWidth; indexX++) {
          fwrite(&(int8_t){getCorrespondingPixel(list, indexX, indexY,
                                                 blocksPerLine, true)},
                 sizeof(int8_t), 1, outFile);
        }
      }

    } else {
      // MODE ASCII
      // headers
      fprintf(outFile, "%s\n%d %d\n%d\n", "P2", imageWidth, imageHeight, 255);

      for (uint32_t indexY = 0; indexY < imageHeight; indexY++) {
        for (uint32_t indexX = 0; indexX < imageWidth; indexX++) {
          fprintf(outFile, "%d ", getCorrespondingPixel(list, indexX, indexY,
                                                        blocksPerLine, true));
        }
        // Nouvelle ligne
        fprintf(outFile, "\n");
      }
    }
  } else {
    if (mode == BINARY) {
      // headers
      fprintf(outFile, "%s\n%d %d\n%d\n", "P6", imageWidth, imageHeight, 255);
      for (uint32_t indexY = 0; indexY < imageHeight; indexY++) {
        for (uint32_t indexX = 0; indexX < imageWidth; indexX++) {
          uint32_t pixel = (uint32_t)getCorrespondingPixel(
              list, indexX, indexY, blocksPerLine, false);
          uint8_t breakDown[3] = {pixel >> 24, (pixel >> 16) & 0xFF,
                                  (pixel >> 8) & 0xFF};
          fwrite(&breakDown, sizeof(uint8_t), 3, outFile);
        }
      }
    } else {
      // headers
      fprintf(outFile, "%s\n%d %d\n%d\n", "P3", imageWidth, imageHeight, 255);
      for (uint32_t indexY = 0; indexY < imageHeight; indexY++) {
        for (uint32_t indexX = 0; indexX < imageWidth; indexX++) {
          uint32_t pixel = (uint32_t)getCorrespondingPixel(
              list, indexX, indexY, blocksPerLine, false);
          fprintf(outFile, "%d %d %d ", pixel >> 24, (pixel >> 16) & 0xFF,
                  (pixel >> 8) & 0xFF);
        }
        // Nouvelle ligne
        fprintf(outFile, "\n");
      }
    }
  }

  fclose(outFile);
}

extern void readPPM(const char *filename, struct colorBlock_t ***list,
                    uint16_t *blocksPerLine, uint16_t *blocksPerColumn,
                    uint16_t *imageWidth, uint16_t *imageHeight, bool *isGray) {
  // Fichier
  if( !strstr(filename, ".ppm") && !strstr(filename, ".pgm") && !strstr(filename, ".pbm" )){
    fatalError("This file is not a .ppm, .pbm, or .pgm file");
  }
  FILE *inFile = fopen(filename, "r");
  if (inFile == NULL) {
    fatalError(strerror(errno));
  }
  uint16_t maxColor;
  char mode[3];
  char buffer[255];
  // Mode (P[\d])
  fscanf(inFile, "%s\n", mode);
  if(strncmp(mode, "P", 1) && strncmp(mode, "p", 1)){
    fatalError("This is not a valid file ! Header is corrupted");
  }
  // Sauter les commentaires
  do {
    fgets(buffer, sizeof(buffer), inFile);
  } while (buffer[0] == '#');
  // Taille
  if( sscanf(buffer, "%hd %hd\n", imageWidth, imageHeight) != 2){
    fatalError("This is not a valid file ! Header is corrupted");
  }
  fgets(buffer, sizeof(buffer), inFile);
  // Couleur
  if( sscanf(buffer, "%hd\n", &maxColor) != 1) {
    fatalError("This is not a valid file ! Header is corrupted");
  }

  // paramètres
  bool isBinary = false;
  bool isGrayScale = false;
  if (strcmp(mode, "P3") > 0) {
    isBinary = true;
  }
  if ((isBinary && !strcmp(mode, "P5")) || (!isBinary && !strcmp(mode, "P2"))) {
    isGrayScale = true;
  }
  // Eclatement en blocs.
  uint16_t nbBlocksWidth = (*imageWidth) / BLOCK_SIZE;
  uint16_t modBlocksWidth = (*imageWidth) % BLOCK_SIZE;
  uint16_t nbBlocksHeight = (*imageHeight) / BLOCK_SIZE;
  uint16_t modBlocksHeight = (*imageHeight) % BLOCK_SIZE;
  *blocksPerLine = (nbBlocksWidth + ((modBlocksWidth == 0) ? 0 : 1));
  *blocksPerColumn = (nbBlocksHeight + ((modBlocksHeight == 0) ? 0 : 1));
  uint32_t nbBlocksTotal = (nbBlocksWidth + ((modBlocksWidth == 0) ? 0 : 1)) *
                           (nbBlocksHeight + ((modBlocksHeight == 0) ? 0 : 1));

  *list = calloc(nbBlocksTotal, sizeof(struct colorBlock_t *));
  if(!(*list)){
    fatalError("Failed to allocate memory");
  }
  for (uint32_t i = 0; i < nbBlocksTotal; i++) {
    (*list)[i] = make_colorBlock(isGrayScale);
  }
  // Affectation des valeurs

  uint8_t* buffers = calloc(nbBlocksTotal , sizeof(uint8_t));
  //On utilise le buffer le plus gros dans tous les cas pour éviter un malloc.
  uint8_t temp[BLOCK_SIZE*3] = {0};
  uint8_t modifier = isGrayScale ? 1 : 3;
  for (uint32_t i = 0; i < (*imageHeight); i++) {
    for (uint32_t j = 0; j < (*imageWidth); j += BLOCK_SIZE) {
      uint32_t indexBlock =
          (j / BLOCK_SIZE) + (*blocksPerLine) * (i / BLOCK_SIZE);
      // Si il reste moins de 8 octets à lire sur la ligne
      uint8_t bytesToRead = ((((*imageWidth) - j) < BLOCK_SIZE) ? ((*imageWidth) - j) : BLOCK_SIZE) * modifier;
      if(isBinary){
        if(fread(temp, sizeof(uint8_t), bytesToRead, inFile) != bytesToRead){
          fatalError("Unexpected EOF.Aborting");
        }
      }else{
        for(uint8_t pixInc=0; pixInc < bytesToRead; pixInc++){
          if(fscanf(inFile, "%d", (int *)&temp[pixInc]) !=1){
            fatalError("Unexpected EOF.Aborting");
          }
        }
      }
      if(isGrayScale){
        for(uint8_t copyInc =0; copyInc < bytesToRead; buffers[indexBlock]++, copyInc ++){
          (*list)[indexBlock]->a->data[buffers[indexBlock]] = temp[copyInc];
        }
      }else{
        for(uint8_t copyInc =0; copyInc < bytesToRead ; buffers[indexBlock]++, copyInc +=3){
          (*list)[indexBlock]->a->data[buffers[indexBlock]] = temp[copyInc];
          (*list)[indexBlock]->b->data[buffers[indexBlock]] = temp[copyInc + 1];
          (*list)[indexBlock]->c->data[buffers[indexBlock]] = temp[copyInc + 2];
        }
      }
      buffers[indexBlock] += (modifier*BLOCK_SIZE - bytesToRead)/modifier;


    }
  }
  free(buffers);
  fclose(inFile);
  *isGray = isGrayScale;
}
