#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "blockFactory.h"

bool assert_block8(struct ublock8_t* block, uint32_t nb) {
    if(block == NULL)
        return false;

    for(uint32_t i = 0; i < nb; i++) {
        if(block->data[i] != 0)
            return false;
    }

    return true;
}

bool assert_block16(struct block16_t* block, uint32_t nb) {
    if(block == NULL)
        return false;

    for(uint32_t i = 0; i < nb; i++) {
        if(block->data[i] != 0)
            return false;
    }

    return true;
}

int main(){
    struct ublock8_t *block = make_ublock8();
    printf("Création d'un bloc 8 bits de taille 64\n");
    if(!assert_block8(block, 64)) {
        printf("Le bloc n'a pas été créé correctement\n");
        return EXIT_FAILURE;
    } else {
        printf("Le bloc a été créé correctement\n");
    }

    struct block16_t *block2 = make_block16();
    printf("Création d'un bloc 16 bits de taille 64\n");
    if(!assert_block16(block2, 64)) {
        printf("Le bloc n'a pas été créé correctement\n");
        return EXIT_FAILURE;
    } else {
        printf("Le bloc a été créé correctement\n");
    }

    printf("Libération des 2 blocs\n");
    free_ublock8(block);
    free_block16(block2);
    printf("Test terminé, relancez le test avec valgrind pour vérifier l'état de la mémoire\n");

    return EXIT_SUCCESS;
}
