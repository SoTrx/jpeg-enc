#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "debug.h"
#include "ppm.h"
#include "JpegCst.h"

int main(){
    /*struct colorBlock_t* invaderInput = malloc(sizeof(struct colorBlock_t));
    invaderInput->a = malloc(sizeof(struct ublock8_t));
    invaderInput->b = NULL;
    invaderInput->c = NULL;

    uint8_t dataInvader[64] = {0, 0, 0, 255, 255, 0, 0, 0,
    0, 0, 255, 255, 255, 255, 0, 0,
    0, 255, 255, 255, 255, 255, 255, 0,
    255, 255, 0, 255, 255, 0, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    0, 0, 255, 0, 0, 255, 0, 0,
    0, 255, 0, 255, 255, 0, 255, 0,
    255, 0, 255, 0, 0, 255, 0, 255};

    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        invaderInput->a->data[i] = dataInvader[i];
    }*/
    struct colorBlock_t ** list = NULL;
    uint16_t blocksPerLine, blocksPerColumn, imageWidth, imageHeight;
    bool isGrayScale;
    readPPM("test.ppm", &list, &blocksPerLine, &blocksPerColumn, &imageWidth, &imageHeight, &isGrayScale);
    writePPM("invader.ppm", list, imageHeight, imageWidth, blocksPerLine, BINARY);
    //readPPM("./images/thumbs.ppm", NULL, 8);
        for(uint32_t i=0; i<blocksPerLine * blocksPerColumn; i++){
            //printf("Libération du bloc %d\n", i);
            free_colorBlock(list[i]);

        }
        free(list);
        list = NULL;

    return EXIT_SUCCESS;
}
