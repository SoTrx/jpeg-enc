#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "debug.h"
#include "ppm.h"
#include "JpegCst.h"

int main(){
    struct colorBlock_t* invaderInput = malloc(sizeof(struct colorBlock_t));
    invaderInput->a = malloc(sizeof(struct block16_t));
    invaderInput->b = NULL;
    invaderInput->c = NULL;

    uint8_t dataInvader[64] = {
            0,   0,   0,   255, 255,   0,   0,   0,
            0,   0,   255, 255, 255, 255,   0,   0,
            0,   255, 255, 255, 255, 255, 255,   0,
            255, 255,   0, 255, 255,   0, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255,
            0,   0,   255,   0,   0, 255,   0,   0,
            0,   255,   0, 255, 255,   0, 255,   0,
            255, 0,   255,   0,   0, 255,   0, 255
    };

    for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
        invaderInput->a->data[i] = dataInvader[i];
    }
    
    printf("Ecriture dans le fichier invader.ppm\n");
    writePPM("invader.ppm", &invaderInput, 8, 8, 1, BINARY);
    printf("Test terminé, vérifiez l'intégrité des données dans le fichier 'invader.ppm'\n");

    free(invaderInput->a);
    free(invaderInput);

    return EXIT_SUCCESS;
}
