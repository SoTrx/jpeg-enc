#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "acdc.h"
#include "JpegCst.h"
#include "jpeg_reader.h"

/**
 * Affiche un vecteur de bloc de manière formattée
 * sous forme de bloc de 8x8
 *
 * @param int16_t* Le vecteur à afficher
 */
static void printVectorToBlock(int16_t *vector) {
	for(uint16_t i = 0; i < BLOCK_SIZE; i++) {
		for(uint16_t j = 0; j < BLOCK_SIZE; j++) {
			printf("%04x ", vector[i * BLOCK_SIZE + j] & 0xFFFF);
		}
		printf("\n");
	}
}

/**
 * Indique si deux vecteur de blocs sont égaux
 *
 * @param int16_t* Premier vecteur
 * @param int16_t* Second vecteur
 * @return bool true si les deux vecteurs sont égaux, false sinon
 */
static bool equalsVector(int16_t *v1, int16_t *v2) {
	for(uint16_t i = 0; i < COMPONENT_SIZE; i++) {
		if(v1[i] != v2[i]) return false;
	}
	return true;
}

int main() {
	struct jpeg_desc *jdesc  = read_jpeg("images/invader.jpeg");
	struct bitstream *stream = get_bitstream(jdesc);
	struct huff_table* tableAC = get_huffman_table(jdesc, AC, 0);
	struct huff_table* tableDC = get_huffman_table(jdesc, DC, 0);

	struct samplingFactors_t factors = { 1, 1, 1, 0, 0, 0, 0, 0, 0 };
	uint8_t real_indexes[1] = {0};

	struct decodeStream_t *decode = getDecodeStream(&tableAC, &tableDC, 1, real_indexes, stream, &factors);

	struct blockMCU_t *mcu = decodeMCU(decode);

	int16_t expected[64] = {
		0x007C, 0x0000, 0xFFFA, 0xFEB3, 0x0000, 0xFEE4, 0x0000, 0xFEDB,
		0x0000, 0xFF36, 0x0080, 0x0000, 0xFF8B, 0x0000, 0x0000, 0x0000,
		0x006A, 0x0000, 0xFFA0, 0x0000, 0xFFA6, 0xFF76, 0x0000, 0x011C,
		0x0000, 0x0045, 0x0000, 0xFFEC, 0x0000, 0xFF7D, 0x0000, 0x0019,
		0x0000, 0x0013, 0x0000, 0x00DD, 0x0000, 0x001A, 0x0000, 0x00FF,
		0x0000, 0x009A, 0x0000, 0x0000, 0x0004, 0x0000, 0x007D, 0x0000,
		0xFFA8, 0x0000, 0xFF59, 0x0000, 0x0014, 0x0000, 0x0000, 0xFE1F,
		0x0000, 0xFFB9, 0x0000, 0x00F4, 0x0000, 0x0000, 0xFF3C, 0x0000
	};

	printf("Valeur retournée : \n");
	printVectorToBlock(mcu->y[0].data);
	printf("Valeur attendue : \n");
	printVectorToBlock(expected);

	bool equals = equalsVector(expected, mcu->y[0].data);
	printf("%s\n", (equals) ? "Les blocs sont égaux" : "Les blocs ne sont pas égaux");

	free(mcu->y);
	free(mcu);
	free(decode);
	close_jpeg(jdesc);

	return EXIT_SUCCESS;
}
