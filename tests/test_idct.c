#include <stdlib.h>
#include <stdio.h>

#include "debug.h"
#include "idct.h"

/**
 * Affiche un vecteur de bloc de manière formattée
 * sous forme de bloc de 8x8
 *
 * @param int8_t* Le vecteur à afficher
 */
static void printVectorToBlock8(uint8_t *vector) {
    for(uint16_t i = 0; i < BLOCK_SIZE; i++) {
        for(uint16_t j = 0; j < BLOCK_SIZE; j++) {
            printf("%4u ", vector[i * BLOCK_SIZE + j]);
        }
        printf("\n");
    }
}

/**
 * Affiche un vecteur de bloc de manière formattée
 * sous forme de bloc de 8x8
 *
 * @param int16_t* Le vecteur à afficher
 */
static void printVectorToBlock16(int16_t *vector) {
    for(uint16_t i = 0; i < BLOCK_SIZE; i++) {
        for(uint16_t j = 0; j < BLOCK_SIZE; j++) {
            printf("%4i ", vector[i * BLOCK_SIZE + j]);
        }
        printf("\n");
    }
}

int main(){
    int passed = EXIT_SUCCESS;
    struct block16_t block_origine;

    int16_t data_origin[64] = {
        0xe7,   0x158,  0xff6e, 0xf,    0,      0x10,   0xfff6, 0,
        0x106,  0xff16, 0x12,   0x54,   0xffd3, 0,      0,      0xb,
        0xffeb, 0xffdc, 0x6f,   0xffa1, 0x10,   0x16,   0xfff2, 0,
        0xfff4, 0x9,    0xfff8, 0xffe8, 0x32,   0xffde, 0x10,   0xc,
        0x8,    0,      0xffeb, 0x16,   0xfff2, 0xffea, 0x15,   0xfff1,
        0xfff6, 0x7,    0xb,    0,      0xfff0, 0x15,   0,      0,
        0,      0,      0xfff0, 0,      0x15,   0,      0,      0x14,
        0,      0,      0,      0,      0,      0,      0,      0
    };

    for (uint8_t i = 0; i < 64; i++) {
        block_origine.data[i] = data_origin[i];
    }

    printf("== Transformée inverse ==\n");

    idct(&block_origine);

    uint8_t data_expected[64] = {
        0xc4, 0xbf, 0xca, 0xc1, 0xcb, 0xc5, 0xc7, 0xc6,
        0xc4, 0xcd, 0xc1, 0xc7, 0xc4, 0xc8, 0xcf, 0xa4,
        0xcb, 0xc1, 0xc6, 0xcd, 0xd2, 0xc8, 0xbb, 0x54,
        0xc6, 0xcc, 0xc7, 0xc7, 0xc9, 0xd6, 0x7a, 0x18,
        0xc0, 0xc8, 0xc5, 0xc9, 0xd4, 0xa0, 0x29, 0x12,
        0xc5, 0xb4, 0xc6, 0xc6, 0xae, 0x39, 0x0d, 0x15,
        0xc4, 0xc4, 0xc3, 0xb5, 0x4f, 0x13, 0x19, 0x14,
        0xbc, 0xc6, 0xc1, 0xb2, 0x4c, 0x14, 0x12, 0x17
    };

    printf("Valeur attendue (+- 1) :\n");
    printVectorToBlock8(data_expected);
    printf("Valeur retournée :\n");
    printVectorToBlock16(block_origine.data);

    for (uint8_t i = 0; i < 32; i++) {
        if (abs(block_origine.data[i] - data_expected[i]) > 1) {
            printf("Les deux blocs ne sont pas égaux\n");
            passed = EXIT_FAILURE;
            break;
        }
    }

    printf("Les deux blocs sont égaux\n\n");

    struct block16_t* block_after_idct = make_block16();
    for (uint8_t i = 0; i < 64; i++) {
        block_after_idct->data[i] = block_origine.data[i];
    }

    printf("== Transformée ==\n");

    dct(block_after_idct);

    printf("Valeur attendue (+- 1) :\n");
    printVectorToBlock16(data_origin);
    printf("Valeur retournée :\n");
    printVectorToBlock16(block_after_idct->data);

    for (uint8_t i = 0; i < 32; i++) {
        if (abs(block_after_idct->data[i] - data_origin[i]) > 1) {
            printf("Les deux blocs ne sont pas égaux\n");
            passed = EXIT_FAILURE;
            break;
        }
    }

    printf("Les deux blocs sont égaux\n\n");

    printf("== Transformée inverse Loeffler ==\n");

    idct_loeffler(block_after_idct);

    printf("Valeur attendue (+- 1) :\n");
    printVectorToBlock8(data_expected);
    printf("Valeur retournée :\n");
    printVectorToBlock16(block_after_idct->data);

    for (uint8_t i = 0; i < 32; i++) {
        /* Il peut il y avoir un écart dépendant de l'algorithme utilisé */
        if (abs(block_after_idct->data[i] - data_expected[i]) > 1) {
            printf("Les deux blocs ne sont pas égaux\n");
            passed = EXIT_FAILURE;
            break;
        }
    }

    printf("Les deux blocs sont égaux\n\n");

    printf("== Transformée Loeffler ==\n");

    dct_loeffler(block_after_idct);

    printf("Valeur attendue (+- 1) :\n");
    printVectorToBlock16(data_origin);
    printf("Valeur retournée :\n");
    printVectorToBlock16(block_after_idct->data);

    for (uint8_t i = 0; i < 32; i++) {
        /* Il peut il y avoir un écart dépendant de l'algorithme utilisé */
        if (abs(block_after_idct->data[i] - data_origin[i]) > 1) {
            printf("Les deux blocs ne sont pas égaux\n");
            passed = EXIT_FAILURE;
            break;
        }
    }

    printf("Les deux blocs sont égaux\n\n");

    free(block_after_idct);

    return passed;
}
