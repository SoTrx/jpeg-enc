#include <stdio.h>
#include <stdlib.h>
#include "bitstream.h"

int main() {
    // Initialisation d'un fichier 'fake' contenant
    FILE* f = fopen("bitstream.temp", "w");
    uint8_t fakeData[8] = {0xa3, 0x04, 0x5b, 0xff, 0x00, 0xff, 0x00, 0xcd};
    fwrite(fakeData, sizeof(uint8_t), 8, f);
    fclose(f);
    printf("Données écrites : A3 04 5B FF 00 FF 00 CD\n\n");

    // Test
    struct bitstream * stream = create_bitstream("bitstream.temp");
    uint32_t buffer = 0;

    read_bitstream(stream, 3, &buffer, true);
    printf("Lecture de 3 bits :\n");
    printf("Valeur attendue :  00000005\n");
    printf("Valeur retournée : %08X\n\n", buffer);
    if (buffer != 0x5) {
      printf("FAIL!\n");
      return EXIT_FAILURE;
    }

    read_bitstream(stream, 13, &buffer, true);
    printf("Lecture de 13 bits :\n");
    printf("Valeur attendue :  00000304\n");
    printf("Valeur retournée : %08X\n\n", buffer);
    if (buffer != 0x304) {
        printf("FAIL!\n");
        return EXIT_FAILURE;
    }

    read_bitstream(stream, 32, &buffer, true);
    printf("Lecture de 32 bits :\n");
    printf("Valeur attendue :  5BFFFFCD\n");
    printf("Valeur retournée : %08X\n\n", buffer);
    if (buffer != 0x5BFFFFCD) {
        printf("FAIL!\n");
        return EXIT_FAILURE;
    }

    printf("Tout s'est bien passé\n");

    remove("bitstream.temp");
    close_bitstream(stream);

    FILE* fichier = fopen("tmp.txt", "w");
    stream = create_write_bitstream(fichier);
    write_bitstream(stream, 5, 0x1F);
    write_bitstream(stream, 3, 0x7);

    end_write(stream);
    close_bitstream(stream);

    return EXIT_SUCCESS;
}
