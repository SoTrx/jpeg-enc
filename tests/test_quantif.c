#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

#include "blockFactory.h"
#include "quantif.h"
#include "debug.h"

int main(){
  struct block16_t* orig = make_block16();
  struct ublock8_t* table = make_ublock8();
  struct block16_t* quant = make_block16();
  struct block16_t* dequant = make_block16();

  time_t t;
  srand((unsigned) time(&t));

  for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
      for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
          orig->data[i + j * BLOCK_SIZE] = 2;
          table->data[i + j * BLOCK_SIZE] = 2;
          quant->data[i + j * BLOCK_SIZE] = 2;
      }
  }

  printf("Matrice d'origine : \n");
  printf_struct_block16_t(orig);
  printf("\n\n\n");

  printf("Table de quantification : \n");
  printf_struct_ublock8_t(table);
  printf("\n\n\n");

  quantif(quant, table->data);
  printf("Matrice quantifiée : \n");
  printf_struct_block16_t(quant);
  printf("\n\n\n");

  for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
          int index = i + j * BLOCK_SIZE;
          assert((orig->data[index] / table->data[index]) == quant->data[index]);
        }
  }

  printf("QUANTIFICATION OK\n");
  printf("\n\n\n");

  /* Copie des résultats de la quantification de quant à dequant */
  for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
      for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
          dequant->data[i + j * BLOCK_SIZE] = quant->data[i + j * BLOCK_SIZE];
      }
  }

  dequantif(dequant, table->data);
  printf("Matrice déquantifiée\n");
  printf_struct_block16_t(dequant);
  printf("\n\n\n");

  for (uint8_t j = 0; j < BLOCK_SIZE; j++) {
        for (uint8_t i = 0; i < BLOCK_SIZE; i++) {
          int index = i + j * BLOCK_SIZE;
          assert((quant->data[index] * table->data[index]) == dequant->data[index] );
        }
    }
  printf("\n\n\n");
  printf("DEQUANTIFICATION OK\n");

  free_ublock8(table);
  free_block16(orig);
  free_block16(quant);
  free_block16(dequant);

  return EXIT_SUCCESS;
}
