#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "dzz.h"
#include "JpegCst.h"
#include "blockFactory.h"

/**
 * Affiche un vecteur de bloc de manière formattée
 * sous forme de bloc de 8x8
 *
 * @param int16_t* Le vecteur à afficher
 */
static void printVectorToBlock(int16_t *vector) {
	for(uint16_t i = 0; i < BLOCK_SIZE; i++) {
		for(uint16_t j = 0; j < BLOCK_SIZE; j++) {
			printf("%3i ", vector[i * BLOCK_SIZE + j]);
		}
		printf("\n");
	}
}

/**
 * Indique si deux vecteur de blocs sont égaux
 *
 * @param int16_t* Premier vecteur
 * @param int16_t* Second vecteur
 * @return bool true si les deux vecteurs sont égaux, false sinon
 */
static bool equalsVector(int16_t *v1, int16_t *v2) {
	for(uint16_t i = 0; i < COMPONENT_SIZE; i++) {
		if(v1[i] != v2[i]) return false;
	}
	return true;
}

int main() {
    // Test zig zag inverse
	printf("== Test zig-zag inverse ==\n");
	// Vecteur zig-zag donné en entrée
	int16_t vector[64] = {
		78, 45, -52, 20, -22, -7, -1, 16,
		-2,  4,  -2, 12, -11,  0,  0,  0,
		 0,  4,   2, -5,  -2,  2,  0,  2,
		-4,  0,   0,  0,   0,  0,  0,  0,
		 2,  2,   0,  0,   0,  0,  0,  2,
		 0,  0,   0,  0,   0,  0,  2,  0,
		 0,  0,   0,  0,   0,  0,  0,  0,
		 0,  0,   0,  0,   0,  0,  0,  0
	};

	// Vecteur dézig-zag attendu en sortie
	int16_t expected[64] = {
		 78,  45,  -7, -1, 0, 0, 0, 0,
		-52, -22,  16,  0, 0, 0, 0, 0,
		 20,  -2, -11,  4, 0, 0, 0, 0,
		  4,  12,   2, -4, 0, 0, 0, 0,
		 -2,  -5,   2,  2, 2, 0, 0, 0,
		 -2,   0,   2,  0, 2, 0, 0, 0,
		  2,   0,   0,  0, 0, 0, 0, 0,
		  0,   0,   0,  0, 0, 0, 0, 0
	};

	struct block16_t *block_origine = malloc(sizeof(struct block16_t));

	for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
		block_origine->data[i] = vector[i];
	}

	inverseZigZag(block_origine);

	printf("Valeur attendue : \n");
	printVectorToBlock(expected);

	printf("Valeur retournée : \n");
	printVectorToBlock(block_origine->data);

	bool return_value = EXIT_SUCCESS;
	bool equals = equalsVector(block_origine->data, expected);
	char *message = (equals)
				  ? "Les deux blocs sont égaux"
				  : "Les deux blocs sont différents";
	printf("%s\n", message);

	if (!equals) {
		return_value = EXIT_FAILURE;
	}

	for (uint8_t i = 0; i < COMPONENT_SIZE; i++) {
		block_origine->data[i] = vector[i];
	}

    // Test zig zag
	printf("\n== Test zig-zag ==\n");

	inverseZigZag(block_origine);
	zigZag(block_origine);

	printf("Valeur attendue : \n");
	printVectorToBlock(vector);

	printf("Valeur retournée : \n");
	printVectorToBlock(block_origine->data);

	equals = equalsVector(block_origine->data, vector);
	message = (equals)
				  ? "Les deux blocs sont égaux"
				  : "Les deux blocs sont différents";
	printf("%s\n", message);

	if (!equals) {
		return_value = EXIT_FAILURE;
	}

	free_block16(block_origine);

	return return_value;
}
