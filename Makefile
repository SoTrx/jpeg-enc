# Repertoires du projet

BIN_DIR  = bin
SRC_DIR  = src
INC_DIR  = include
OBJ_DIR  = obj
TEST_DIR = tests
OBJPROF_DIR = obj-prof

# Options de compilation/édition des liens

CC = clang
INC = -I$(INC_DIR)

CFLAGS += $(INC) -Wall -std=c99 -O3 -Wextra
LDFLAGS = -lm -L . -ljpeg
#-L . -ljpeg

all: libjpeg.a $(BIN_DIR)/jpeg2ppm $(BIN_DIR)/ppm2jpeg $(BIN_DIR)/test_writeppm $(BIN_DIR)/test_acdc $(BIN_DIR)/test_bitstream $(BIN_DIR)/test_blockFactory $(BIN_DIR)/test_dzz $(BIN_DIR)/test_idct $(BIN_DIR)/test_quantif $(BIN_DIR)/test_readppm
	@play sanic.ogg > /dev/null 2>&1 &
	@echo -e '\033[0;34m'
	@echo ░░░░░░░░░▄▄▄▄▄
	@echo ░░░░░░░░▀▀▀██████▄▄▄
	@echo ░░░░░░▄▄▄▄▄░░█████████▄
	@echo ░░░░░▀▀▀▀█████▌░▀▐▄░▀▐█
	@echo ░░░▀▀█████▄▄░▀██████▄██
	@echo ░░░▀▄▄▄▄▄░░▀▀█▄▀█════█▀
	@echo ░░░░░░░░▀▀▀▄░░▀▀███░▀░░░░░░▄▄
	@echo ░░░░░▄███▀▀██▄████████▄░▄▀▀▀██▌
	@echo ░░░██▀▄▄▄██▀▄███▀░▀▀████░░░░░▀█▄
	@echo ▄▀▀▀▄██▄▀▀▌████▒▒▒▒▒▒███░░░░▌▄▄▀
	@echo ▌░░░░▐▀████▐███▒▒▒▒▒▐██▌
	@echo ▀▄░░▄▀░░░▀▀████▒▒▒▒▄██▀
	@echo ░░▀▀░░░░░░▀▀█████████▀
	@echo ░░░░░░░░▄▄██▀██████▀█
	@echo ░░░░░░▄██▀░░░░░▀▀▀░░█
	@echo ░░░░░▄█░░░░░░░░░░░░░▐▌
	@echo ░▄▄▄▄█▌░░░░░░░░░░░░░░▀█▄▄▄▄▀▀▄
	@echo ▌░░░░░▐░░░░░░░░░░░░░░░░▀▀▄▄▄▀
	@echo GOTTA GO FAST !
	@echo -e '\033[0m'

$(BIN_DIR)/jpeg2ppm: $(OBJ_DIR)/jpeg2ppm.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/ppm2jpeg: $(OBJ_DIR)/ppm2jpeg.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_writeppm: $(OBJ_DIR)/test_writePPM.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_acdc: $(OBJ_DIR)/test_acdc.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_bitstream: $(OBJ_DIR)/test_bitstream.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_blockFactory: $(OBJ_DIR)/test_blockFactory.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_dzz: $(OBJ_DIR)/test_dzz.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_idct: $(OBJ_DIR)/test_idct.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_quantif: $(OBJ_DIR)/test_quantif.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@

$(BIN_DIR)/test_readppm: $(OBJ_DIR)/test_readPPM.o libjpeg.a
	$(CC) $< $(LDFLAGS) -o $@


libjpeg.a: $(OBJ_DIR)/acdc.o $(OBJ_DIR)/bitstream.o $(OBJ_DIR)/blockFactory.o $(OBJ_DIR)/color_converter.o $(OBJ_DIR)/debug.o $(OBJ_DIR)/dzz.o $(OBJ_DIR)/huffman.o $(OBJ_DIR)/idct.o $(OBJ_DIR)/jpeg_reader.o $(OBJ_DIR)/quantif.o $(OBJ_DIR)/sampler.o $(OBJ_DIR)/ppm.o
	ar rcs libjpeg.a $^

$(OBJ_DIR)/acdc.o: $(INC_DIR)/bitstream.h $(INC_DIR)/huffman.h

$(OBJ_DIR)/color_converter.o: $(INC_DIR)/blockFactory.h

$(OBJ_DIR)/dzz.o: $(INC_DIR)/JpegCst.h

$(OBJ_DIR)/jpeg_reader.o: $(INC_DIR)/huffman.h $(INC_DIR)/JpegCst.h

$(OBJ_DIR)/ppm.o: $(INC_DIR)/JpegCst.h $(INC_DIR)/blockFactory.h

$(OBJ_DIR)/quantif.o: $(INC_DIR)/JpegCst.h $(INC_DIR)/blockFactory.h

$(OBJ_DIR)/jpeg2ppm.o: $(INC_DIR)/argParse.h $(INC_DIR)/debug.h $(INC_DIR)/acdc.h $(INC_DIR)/huffman.h $(INC_DIR)/jpeg_reader.h $(INC_DIR)/bitstream.h $(INC_DIR)/idct.h $(INC_DIR)/color_converter.h $(INC_DIR)/quantif.h $(INC_DIR)/dzz.h $(INC_DIR)/ppm.h $(INC_DIR)/sampler.h

$(OBJ_DIR)/ppm2jpeg.o: $(INC_DIR)/argParse.h $(INC_DIR)/debug.h $(INC_DIR)/acdc.h $(INC_DIR)/huffman.h $(INC_DIR)/jpeg_reader.h $(INC_DIR)/bitstream.h $(INC_DIR)/idct.h $(INC_DIR)/color_converter.h $(INC_DIR)/quantif.h $(INC_DIR)/dzz.h $(INC_DIR)/ppm.h $(INC_DIR)/sampler.h

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ_DIR)/%.o: $(TEST_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean

clean:
	rm -f $(BIN_DIR)/jpeg2ppm $(BIN_DIR)/ppm2jpeg $(BIN_DIR)/test_* $(OBJ_DIR)/* libjpeg.a
