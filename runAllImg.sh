#!/bin/bash

imgs=(invader.jpeg gris.jpg bisou.jpeg albert.jpg zig-zag.jpg thumbs.jpg horizontal.jpg vertical.jpg shaun_the_sheep.jpeg complexite.jpeg truc.jpg truc2.jpg truc4.jpg truc5.jpg truc6.jpg)

for img in ${imgs[@]}; do
    if [ "$1" = "-v" ]; then
        valgrind --leak-check=full --error-exitcode=1 bin/jpeg2ppm "images/$img" 2> /dev/null

        if [ "$?" -eq 1 ]; then
            echo -e "\033[0;31m[ERROR] \033[0mvalgrind fichier : $img"
            exit 1
        else
            echo -e "\033[0;32m[PASS] \033[0mvalgrind fichier : $img"
        fi
    else
        bin/jpeg2ppm "images/$img"
    fi
done
