#ifndef __BLOCKFACTORY_H__
#define __BLOCKFACTORY_H__
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "JpegCst.h"

struct ublock8_t {
	uint8_t data[COMPONENT_SIZE];
};

struct block16_t {
	int16_t data[COMPONENT_SIZE];
};

struct colorBlock_t {
	struct block16_t* a;
	struct block16_t* b;
	struct block16_t* c;
};

/**
 * Structure contenant les facteurs d'échantillonnage
 *
 *
 * hY : nombre de blocs 8x8 Y à l'horizontale / MCU
 * vY : nombre de blocs 8x8 Y à la verticale / MCU
 * nbY = hY * vY : nombre de blocs 8x8 Y / MCU
 * hCb : nombre de blocs 8x8 Cb à l'horizontale / MCU
 * vCb : nombre de blocs 8x8 Cb à la verticale / MCU
 * nbCb = hCb * vCb : nombre de blocs 8x8 Cb / MCU
 * hCr : nombre de blocs 8x8 Cr à l'horizontale / MCU
 * vCr : nombre de blocs 8x8 Cr à la verticale / MCU
 * nbCr = hCr * vCr : nombre de blocs 8x8 Cr / MCU
 */
struct samplingFactors_t {
    uint8_t hY, vY, nbY, hCb, vCb, nbCb, hCr, vCr, nbCr;
};

struct blockMCU_t {
	struct block16_t* y;
	struct block16_t* cb;
	struct block16_t* cr;
};

struct blockMCU8_t {
	struct ublock8_t* y;
	struct ublock8_t* cb;
	struct ublock8_t* cr;
};

/**
 * Malloc NULL-proof
 * @param dataSize Taille des données à allouer
 * @return pointeur sur la zone mémoire
 */
static inline void* smalloc(size_t dataSize);

/**
 * Alloue et renvoie un pointeur sur le block. Si la taille des données
 * est passée en paramètre, les données sont aussi initialisées.
 * @return            Pointeur sur le bloc
 */
extern struct ublock8_t* make_ublock8();

/**
 * Libère le bloc passé en paramètre
 * @param block Block à libérer
 */
extern void free_ublock8(struct ublock8_t* block);

/**
 * Alloue et renvoie un pointeur sur le block. Si la taille des données
 * est passée en paramètre, les données sont aussi initialisées.
 * @return            Pointeur sur le bloc
 */
extern struct block16_t* make_block16();

/**
 * Libère le bloc passé en paramètre
 * @param block Block à libérer
 */
extern void free_block16(struct block16_t* block);

/**
 * Libère la liste de blocs passée en paramètre
 * @param blocks Liste de blocks à libérer
 */
extern void free_nblock16(struct block16_t* blocks);

/**
 * Alloue et renvoie un pointeur sur le block. Si la taille des données
 * est passée en paramètre, les données sont aussi initialisées.
 * @param  isGrayScale Si le bloc représente une image en nuance de gris, seul r est alloué,
 *                     g et b pointent alors vers NULL
 * @return            Pointeur sur le bloc
 */
extern struct colorBlock_t* make_colorBlock(bool isGrayScale);

/**
 * Libère le bloc passé en paramètre
 * @param block Block à libérer
 */
extern void free_colorBlock(struct colorBlock_t* block);

/**
 * Libère le MCU passé en paramètre
 * @param block MCU à libérer
 */
extern void free_blockMCU(struct blockMCU_t* block);

#endif
