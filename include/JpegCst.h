#ifndef __JPEGCST_H__
#define __JPEGCST_H__

#include <stdint.h>

#define COMPONENT_SIZE 64
#define BLOCK_SIZE 8
#define BLOCK_SIZE2 16

#define EOB 0x00
#define ZRL 0xF0
#define SOF0 0xFFC0
#define DHT 0xFFC4
#define SOI 0xFFD8
#define EOI 0xFFD9
#define SOS 0xFFDA
#define DQT 0xFFDB
#define APP0 0xFFE0

#define PI 3.141592654f
#define SQRT2 1.414213562f
#define SQRT2INVERT 0.7071067812f

#define LOEFFLER_CST1 0.541196100f /* sqrt(2) * cos(6*PI/16) */
#define LOEFFLER_CST2 1.306562965f /* sqrt(2) * sin(6*PI/16) */
#define LOEFFLER_CST3 0.831469612f /* cos(3*PI/16) */
#define LOEFFLER_CST4 0.555570233f /* sin(3*PI/16) */
#define LOEFFLER_CST5 0.980785280f /* cos(PI/16) */
#define LOEFFLER_CST6 0.195090322f /* sin(PI/16) */

#define LIMIT_UINT32 4294967296

#endif
