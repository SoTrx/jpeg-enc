#ifndef __SAMPLER_H__
#define __SAMPLER_H__

#include "blockFactory.h"
#include "JpegCst.h"

/**
 * Vérifie si les facteurs d'échantillonnage passés en paramètres sont
 * supportés par le logiciel de décodage actuel
 *
 * @param struct samplingFactors_t*     Facteurs d'échantillonnage
 * @return bool                         True si supportés
 */
extern bool checkSamplingFactors(struct samplingFactors_t* factors);

/**
 * Sur-échantillonne un bloc à partir des paramètre de
 * sous-échantillonnage
 *
 * @param struct blockMCU_t* Le bloc à sur-échantillonner
 * @param struct samplingFactors_t* Les facteurs d'échantillonnage
 */
extern void sampler_upSample(
    struct blockMCU_t*,
    struct samplingFactors_t*
);

extern void subsample(
    struct blockMCU_t*,
    struct samplingFactors_t*
);

/**
 * Transforme un ensemble de MCU en un ensemble de blocks YCbCr utilisables.
 *
 * @param struct colorBlock_t** Liste de blocs YCbCr ordonnée en sortie
 * @param struct blockMCU_t** La liste de blocs MCU
 * @param uint32_t Le nombre de blocs MCU dans la liste
 * @param struct samplingFactors_t* Les facteurs d'échantillonnage
 * @param uint8_t Le nombre de composantes dans l'image
 * @param uint16_t Le nombre de blocks 8x8 sur la largeur de l'image
 */
extern void sampler_MCUsubdivide(
    struct colorBlock_t**,
    struct blockMCU_t**,
    uint32_t,
    struct samplingFactors_t*,
    uint8_t,
    uint16_t
);

#endif
