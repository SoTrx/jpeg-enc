#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "JpegCst.h"
#include "blockFactory.h"
#include "argParse.h"

#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)                                                   \
  (byte & 0x80 ? '1' : '0'), (byte & 0x40 ? '1' : '0'),                        \
      (byte & 0x20 ? '1' : '0'), (byte & 0x10 ? '1' : '0'),                    \
      (byte & 0x08 ? '1' : '0'), (byte & 0x04 ? '1' : '0'),                    \
      (byte & 0x02 ? '1' : '0'), (byte & 0x01 ? '1' : '0')

enum exitCode {
    TOO_SMALL = 1,
    UNSUPPORTED_COLOR,
    TOO_FEW_BLOCKS,
    EXTENSION_ERROR,
    SAMPLING_ERROR
};
#define sanic() \
printf("░░░░░░░░░▄▄▄▄▄\n"); \
printf("░░░░░░░░▀▀▀██████▄▄▄\n"); \
printf("░░░░░░▄▄▄▄▄░░█████████▄\n"); \
printf("░░░░░▀▀▀▀█████▌░▀▐▄░▀▐█  \n"); \
printf("░░░▀▀█████▄▄░▀██████▄██\n"); \
printf("░░░▀▄▄▄▄▄░░▀▀█▄▀█════█▀\n"); \
printf("░░░░░░░░▀▀▀▄░░▀▀███░▀░░░░░░▄▄\n"); \
printf("░░░░░▄███▀▀██▄████████▄░▄▀▀▀██▌\t\tGOTTA GO FAST !!!!!\n"); \
printf("░░░██▀▄▄▄██▀▄███▀░▀▀████░░░░░▀█▄\n"); \
printf("▄▀▀▀▄██▄▀▀▌████▒▒▒▒▒▒███░░░░▌▄▄▀\n"); \
printf("▌░░░░▐▀████▐███▒▒▒▒▒▐██▌\n"); \
printf("▀▄░░▄▀░░░▀▀████▒▒▒▒▄██▀\n"); \
printf("░░▀▀░░░░░░▀▀█████████▀\n"); \
printf("░░░░░░░░▄▄██▀██████▀█\n"); \
printf("░░░░░░▄██▀░░░░░▀▀▀░░█\n"); \
printf("░░░░░▄█░░░░░░░░░░░░░▐▌\n"); \
printf("░▄▄▄▄█▌░░░░░░░░░░░░░░▀█▄▄▄▄▀▀▄\n"); \
printf("▌░░░░░▐░░░░░░░░░░░░░░░░▀▀▄▄▄▀\n");

#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"
/**
 * Affiche un message d'erreur avec prefixe de la fonction, du fichier, et de la ligne buggée
 * @param message Message perso à afficher
 */
#define fatalError(message) ({ \
  fprintf(stderr, COLOR_RED "[%s : %s() line : %d] => %s .Aborting !" COLOR_RESET "\n", __FILE__,  __FUNCTION__, __LINE__, message); \
  exit(-1); \
})

/**
 * Affiche le contenu d'un ublock8_t
 *
 * @param struct ublock8_t*  Le block à afficher
 */
extern void printf_struct_ublock8_t(struct ublock8_t* block);

/**
 * Affiche le contenu d'un block16_t
 *
 * @param struct block16_t*  Le block à afficher
 */
extern void printf_struct_block16_t(struct block16_t* block);

extern void print_debug(bool isVerbose, char*, ...);

extern void exitIfTrue(bool, char*, enum exitCode);

#endif
