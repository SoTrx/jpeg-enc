#ifndef __PPM_H__
#define __PPM_H__
#include <stdint.h>
#include <stdbool.h>

#include "blockFactory.h"

typedef enum{
  BINARY,
  ASCII,
  NB_WriteMode
} WriteMode;
/**
 * Sequentially write the given blocks list to a PPM file.
 * @param const char* Name of the file to be written
 * @param struct colorBlock_t** Blocks to be written
 * @param uint16_t Height of the image
 * @param uint16_t Width of the image
 * @param uint16_t Number of blocks per line in the image
 */
extern void writePPM(
    const char *filename,
    struct colorBlock_t **list,
    const uint16_t imageHeight,
    const uint16_t imageWidth,
    const uint16_t blocksPerLine,
    const WriteMode mode );

/**
 * Sequentially write the given blocks list to a PPM file.
 * @param const char* Name of the file to be read
 * @param struct colorBlock_t** Catalyser. Must be allocated.
 * @param uint16_t Height of the image
 * @param uint16_t Width of the image
 * @param uint16_t Number of blocks per line in the image
 */
extern void readPPM(
    const char *filename,
    struct colorBlock_t ***list,
    uint16_t * blocksPerLine,
    uint16_t * blocksPerColumn,
    uint16_t * imageWidth,
    uint16_t * imageHeight,
    bool * isGray
  );



#endif
