#ifndef __IDCT_H__
#define __IDCT_H__

#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "JpegCst.h"
#include "blockFactory.h"

/**
 * Réalise la transformée en cosinus discrète inverse sur le
 * bloc passé en paramètre
 *
 * @param struct block16_t* Le bloc en question
 */
extern void idct(struct block16_t*);

/**
 * Application de l'algorithme de Loeffler sur un block16_t (2D).
 *
 * @param struct block16_t* Le bloc en question
 */
extern void idct_loeffler(struct block16_t*);

/**
 * Réalise la transformée en cosinus discrète sur le bloc
 * passé en paramètre
 *
 * @param struct block16_t* Le bloc en question
 */
extern void dct(struct block16_t*);

/**
 * Application de l'algorithme de Loeffler sur un ublock8_t (2D).
 *
 * @param struct block16_t* Le bloc en question
 */
extern void dct_loeffler(struct block16_t*);

#endif
