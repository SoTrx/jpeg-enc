#ifndef __QUANTIF_H__
#define __QUANTIF_H__
#include "blockFactory.h"
/**
 * Déquantifie le bloc passé en paramètre à partir d'une table
 * de Quantification.
 *
 * @param struct block16_t* Le bloc à déquantifier
 * @param struct uint8_t* La table de déquantification
 */
extern void dequantif(struct block16_t*, uint8_t*);

/**
 * Quantifie le bloc passé en paramètre à partir d'une table
 * de Quantification.
 *
 * @param struct block16_t* Le bloc à quantifier
 * @param struct uint8_t* La table de quantification
 */
extern void quantif(struct block16_t*, uint8_t*);

#endif
