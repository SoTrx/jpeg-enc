#ifndef __ACDC_H__
#define __ACDC_H__

#include <stdint.h>

#include "blockFactory.h"

struct decodeStream_t {
	struct huff_table** tablesAC;
	struct huff_table** tablesDC;
	struct bitstream* stream;
	struct samplingFactors_t* factors;

	uint8_t* real_indexes;
	uint8_t nbComponents;

	int16_t lastDCY;
	int16_t lastDCCr;
	int16_t lastDCCb;
};

/**
 * Réalise le décodage AC/DC à partir d'un flot de données de
 * décodage
 *
 * @param struct decodeStream_t* Le flux de décodage
 * @return struct blockMCU_t* Le MCU décodé
 */
extern struct blockMCU_t* decodeMCU(struct decodeStream_t*);

/**
 * Construit un flux de décodage JPEG
 *
 * @param struct huff_table** 		Le tableaux des tables de Huffman AC
 * @param struct huff_table** 		Le tableaux des tables de Huffman DC
 * @param uint8_t					Le nombre de composantes
 * @param uint8_t*					Les index réels dans le flux SOS
 * @param struct bitstream*			Le flux de lecture binaire
 * @param struct samplingFactors_t* Les facteurs d'échantillonnage
 * @return struct decodeStream_t* Le flux de décodage JPEG
 */
extern struct decodeStream_t* getDecodeStream(
	struct huff_table**,
	struct huff_table**,
	uint8_t,
	uint8_t*,
	struct bitstream*,
	struct samplingFactors_t*);


/**
 * Écrit les MCUs avec encodage AC/DC RLE/Huffman dans le fichier passé en
 * paramètre.
 *
 * @param struct blockMCU_t**		La liste de blocs à écrire
 * @param uint32_t					Le nombre de blocs MCU dans la liste
 * @param struct samplingFactors_t*	Les facteurs d'échantillonnage
 * @param struct bitstream*			Le bitstream pour écrire les données
 * @param struct huff_table**		Liste des tables de Huffman DC
 * @param struct huff_table**		Liste des tables de Huffman AC
 */
extern void encodeAndWriteAllMCU(struct blockMCU_t **blocksMCU,
						  uint32_t nb_mcu,
						  struct samplingFactors_t *factors,
						  struct bitstream *stream,
					  	  struct huff_table **huff_tables_dc,
						  struct huff_table **huff_tables_ac);

#endif
