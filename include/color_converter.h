#ifndef __COLORCONVERTER_H__
#define __COLORCONVERTER_H__

#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "JpegCst.h"
#include "blockFactory.h"

/**
 * Dans le cas d'une image en niveaux de gris le bloc d'origine unique
 * est utilisé comme luminance Y sans conversion.
 * Inversement, le seul bloc de luminance Y peut être utilisé directement
 * comme bloc de niveaux de gris.
 */

/**
 * Convertit le bloc initialement en YCbCr en RGB
 *
 * @param struct colorBlock_t* Le bloc YCbCr initial
 */
extern void convertYCbCrToRGB(struct colorBlock_t*);

/**
 * Convertit le bloc initialement en RGB en YCbCr
 *
 * @param struct colorBlock_t* Le bloc RGB initial
 */
extern void convertRGBToYCbCr(struct colorBlock_t*);

#endif
