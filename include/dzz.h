#ifndef __DZZ_H__
#define __DZZ_H__

#include <stdint.h>

#include "blockFactory.h"


/**
 * Réalise l'étape de zig-zag à l'envers sur les données
 * d'un bloc.
 * Cette fonction est plus rapide que inverseZigZag.
 *
 * @param struct block16_t* Le vecteur/bloc à modifier
 */
extern void inverseZigZag(struct block16_t*);

/**
 * Réalise l'étape de zig-zag sur les données d'un bloc.
 *
 * @param struct block16_t* Le vecteur/bloc à modifier
 */
extern void zigZag(struct block16_t*);

#endif
