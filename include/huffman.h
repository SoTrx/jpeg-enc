#ifndef __HUFFMAN_H__
#define __HUFFMAN_H__

#include <stdint.h>
#include <stdbool.h>
#include "bitstream.h"


struct huff_table;

extern struct huff_table *load_huffman_table(struct bitstream *stream,
                                             uint16_t *nb_byte_read);

extern int8_t next_huffman_value(struct huff_table *table,
                                 struct bitstream *stream);

extern void free_huffman_table(struct huff_table *table);

/**
 * Retourne le code de Huffman correspondant à la valeur fournie dans l'arbre
 *
 * @param struct huff_table*        La table de Huffman à utiliser
 * @param uint16_t                  La valeur à trouver dans l'arbre
 * @param uint8_t*                  La longueur du code de Huffman trouvé
 * @return uint16_t                 Le code de Huffman correspondant
 */
extern uint16_t find_huffman_code(struct huff_table *table,
                                  uint16_t value,
                                  uint8_t *code_length);

/**
 * Charge un arbre de Huffman stocké dans un tableau en mémoire
 *
 * @param uint8_t*          Un pointeur sur l'arbre de Huffman en mémoire
 * @param uint16_t*         Le nombre d'octets lu pour construire l'arbre
 * @return struct huff_table* L'arbre de Huffman construit
 */
extern struct huff_table *load_huffman_table_memory(uint8_t *tab, uint16_t *nb_byte_read);

#endif
