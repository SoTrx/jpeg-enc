#ifndef __BITSTREAM_H__
#define __BITSTREAM_H__

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

struct bitstream;

extern struct bitstream *create_bitstream(const char *filename);

extern void close_bitstream(struct bitstream *stream);

extern uint8_t read_bitstream(struct bitstream *stream,
                              uint8_t nb_bits,
                              uint32_t *dest,
                              bool discard_byte_stuffing);

extern bool end_of_bitstream(struct bitstream *stream);

/* Optionnel! */
extern void skip_bitstream_until(struct bitstream *stream, uint8_t byte);

extern bool read_bitstream_safe(struct bitstream *stream,
                              uint8_t nb_bits,
                              uint32_t *dest,
                              bool discard_byte_stuffing);

/* Ecriture */
extern struct bitstream *create_write_bitstream(FILE *fichier);

extern void write_end_bitsream(struct bitstream *stream);

extern void write_bitstream(struct bitstream *stream, uint8_t nbBits, uint32_t toWrite);

extern void end_write(struct bitstream *stream);

#endif
