#Guimp : Le logiciel bien

![WIP](https://img4.hostingpics.net/thumbs/mini_635981aaaaalogo.png)

##Usage

####Conversion d'images JPEG en .ppm
  ```
  jpeg2ppm FICHIER_SOURCE [-v]
  ```
  Formats supportés :
  + 2x1 2x1 2x1
  + 1x1 0x0 0x0
  + 2x1 1x1 1x1
  + 1x2 2x1 1x1
  + 2x2 1x1 1x1
  + 2x2 1x2 1x2
  + 2x2 2x1 2x1
  + 2x2 2x1 1x2
  + 2x2 1x2 2x1
  + 2x2 1x1 1x2
  + 2x2 2x1 1x1
  + 2x2 1x2 1x1
  + 2x2 1x1 2x1
  + 2x2 2x2 1x1
  + 2x2 1x1 2x2
  + 4x1 2x1 1x1
  + 1x4 1x2 1x1
  + 4x2 1x1 1x1

####Conversion d'images .ppm en JPEG
  ```
  ppm2jpeg FICHIER_SOURCE [-v]
  ```
  **Note:** Convertit en JPEG baseline.
####Liste des options :
  + **-v** : Mode verbose, affiche les étapes sur stdout.

##Arborescence du projet

```bash
.
├── bin
├── docs
├── images
│   └── evils
├── include
├── obj
├── obj-prof
├── src
└── tests
```
__bin :__ Exécutables du projet

__docs :__ Documentations auxiliaires

__images :__ Images de tests
+ __evils :__ images corrompues, ou invalides auxquelles le programme doit répondre de manière adéquate.

__include :__ Répertoire des headers C.

__obj :__ Répertoire des fichiers objets construits par le Makefile.

__obj-prof :__ Répertoire des fichiers objets "permanents" déjà compilés.

__src :__ Sources principales du projet.

__tests :__ Répertoire des tests de modules. Chaque test correspond à un module.

###Fichier particulier

CmakeList.txt est le fichier de configuration de l'utilitaire Cmake (2000) qui permet de générer un Makefile adapté à la station de travail courante. En cas de d'incompatibilité technique ou morale avec Cmake, le fichier makeCleaner permettait de modifier le Makefile généré pour le rendre adaptatif.

Cette méthode n'est plus utilisée par le projet, un Makefile manuel s'étant imposé.


###Auteurs

Bonnaventure William
Peirone Lucas
Turlure Maxime
